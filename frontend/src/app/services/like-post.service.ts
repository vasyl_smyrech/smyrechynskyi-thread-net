import { Injectable } from '@angular/core';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { PostService } from './post.service';
import { User } from '../models/user/user';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { Reaction } from '../models/reactions/reaction';

@Injectable({ providedIn: 'root' })
export class LikePostService {
    public constructor(
        private postService: PostService) {}

    public likePost(post: Post, currentUser: User) {
        return this.setPostReaction(post, currentUser, true);
    }

    public dislikePost(post: Post, currentUser: User) {
        return this.setPostReaction(post, currentUser, false);
    }

    private setPostReaction(post: Post, currentUser: User, isLike: boolean){
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike: isLike,
            userId: currentUser.id
        };

        const oldUserForPostReaction = innerPost.reactions.find((x) => x.user.id === currentUser.id);

        if (oldUserForPostReaction && oldUserForPostReaction.isLike === isLike) {
            innerPost.reactions = innerPost.reactions.filter((x) => x.user.id !== currentUser.id)
        } else if (oldUserForPostReaction) {
            const newUserForPostReaction = oldUserForPostReaction;
            newUserForPostReaction.isLike = !newUserForPostReaction.isLike;
            innerPost.reactions  = innerPost.reactions
            .filter((x) => x.user.id !== currentUser.id)
            .concat(newUserForPostReaction);
        } else {
            innerPost.reactions = innerPost.reactions.concat({ isLike: isLike, user: currentUser });
        }

        return isLike
            ? this.postService.likePost(reaction).pipe(
                map(() => innerPost),
                catchError(() => { return this.revertChanges(innerPost, oldUserForPostReaction, isLike, currentUser); }))
            : this.postService.dislikePost(reaction).pipe(
                map(() => innerPost),
                catchError(() => { return this.revertChanges(innerPost, oldUserForPostReaction, isLike, currentUser); }))
    }

    revertChanges(post: Post, reaction: Reaction, isLike: boolean, user: User) {
 
        if (reaction && reaction.isLike === isLike) {
            post.reactions = post.reactions.concat({ isLike: isLike, user: user });
        } else if (reaction) {
            post.reactions  = post.reactions
            .filter((x) => x.user.id !== user.id)
            .concat(reaction);
        } else {
            post.reactions = post.reactions.filter((x) => x.user.id !== user.id);
        }

        return of(post);
    }
}
