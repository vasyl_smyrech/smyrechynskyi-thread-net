import { Injectable } from '@angular/core';
import { HubConnectionBuilder, HubConnection } from '@aspnet/signalr';
import { SnackBarService } from './snack-bar.service';
import { of, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../models/user/user';

@Injectable({ providedIn: 'root' })
export class PostHubService {
    private postHub: HubConnection;
    private user: User;
    
    public constructor(
        private snackBarService: SnackBarService
    ) {}

    public registerHub() {
        this.postHub = new HubConnectionBuilder()
            .withUrl('https://localhost:44344/notifications/post')
            .build();

        const started = this.postHub
            .start()
            .catch((error) => this.snackBarService.showErrorMessage(error));

        return from(started);
    }

    public get getPostHub() {
        return this.postHub 
        ? of(this.postHub)
        : this.registerHub().pipe(
            map(() => this.postHub));
    }

    public mapUserToConnection(userId: number) {
        if (userId) {
            this.getPostHub.subscribe(
                (res) => res.send('MapUserToConnection', this.user.id.toString()),
                (error) => console.error(error));  
        }
    }

    public excludeUserFromConnections(userId: number) {
        if (userId) {
            this.getPostHub.subscribe(
                (res) => res.send('ExcludeUserFromConnections', userId.toString()),
                (error) => console.error(error));  
        }
    }

    public set setUser(user: User){
        this.user = user;
    }

}
