import { Injectable } from '@angular/core';

import { Subject } from 'rxjs';
import { User } from '../models/user/user';
import { PostHubService } from './post-hub.service';

// tslint:disable:member-ordering
@Injectable({ providedIn: 'root' })
export class EventService {

    constructor(
        private postHubService: PostHubService) {}

    private onUserChanged = new Subject<User>();
    public userChangedEvent$ = this.onUserChanged.asObservable();

    public userChanged(user: User) {
        this.onUserChanged.next(user);
        this.postHubService.setUser = user;
    }
}
