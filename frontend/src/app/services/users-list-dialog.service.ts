import { Injectable, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { UsersListDialogComponent } from '../components/dialogs/users-list-dialog/users-list-dialog.component';
import { Reaction } from '../models/reactions/reaction';

@Injectable({ providedIn: 'root' })
export class UsersListDialogService implements OnDestroy {
    private unsubscribe$ = new Subject<void>();

    public constructor(private dialog: MatDialog) {}

    public openDislikedUsersListDialog(reactions: Reaction[]) {
        const dislikeReactions = reactions.filter(r => !r.isLike);
        this.openUsersListDialog(
            `Disliked by ${dislikeReactions.length} person${dislikeReactions.length === 1 ? "" : "s"}:`,
            dislikeReactions);
    }
    public openLikedUsersListDialog(reactions: Reaction[]) {
        const likeReactions = reactions.filter(r => r.isLike)
        this.openUsersListDialog(
            `Liked by ${likeReactions.length} person${likeReactions.length === 1 ? "" : "s"}:`,
            likeReactions);
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    private openUsersListDialog(title: string, reactions: Reaction[]) {
        const dialog = this.dialog.open(UsersListDialogComponent, {
            data: {
                title: title,
                reactions: reactions 
            },
            minWidth: 300,
            autoFocus: true,
            backdropClass: 'dialog-backdrop',
        });
    }
}
