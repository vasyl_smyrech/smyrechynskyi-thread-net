import { Injectable } from '@angular/core';
import { HttpInternalService } from '../services/http-internal.service';
import { User } from '../models/user/user';
import { ResetPasswordEmailDto } from '../models/user/reset-password-email-dto';
import { ApproveResetPasswordRequestDto } from '../models/user/approve-reset-password-request-dto';
import { UpdateUserPasswordDto } from '../models/user/update-user-password-dto';

@Injectable({ providedIn: 'root' })
export class UserService {
    public routePrefix = '/api/users';

    constructor(private httpService: HttpInternalService) {}

    public getUserFromToken() {
        return this.httpService.getFullRequest<User>(`${this.routePrefix}/fromToken`);
    }

    public getUserById(id: number) {
        return this.httpService.getFullRequest<User>(`${this.routePrefix}`, { id });
    }

    public sendResetPasswordEmail(resetEmailDto: ResetPasswordEmailDto) {
        return this.httpService.postFullRequest<ResetPasswordEmailDto>(`${this.routePrefix}/sendResetUserPasswordEmail`, resetEmailDto);       
    }

    public approveResetPasswordRequest(approveResetPasswordRequestDto: ApproveResetPasswordRequestDto) {
        return this.httpService.headFullRequest<ApproveResetPasswordRequestDto>(`${this.routePrefix}/approveResetUserPasswordRequest`, approveResetPasswordRequestDto);       
    }

    public updateUser(user: User) {
        return this.httpService.putFullRequest<User>(`${this.routePrefix}`, user);
    }

    public updateUserPassword(updateUserPasswordDto: UpdateUserPasswordDto) {
        return this.httpService.putFullRequest<User>(`${this.routePrefix}/updateUserPassword`, updateUserPasswordDto);
    }

    public copyUser({ avatar, email, userName, id }: User) {
        return {
            avatar,
            email,
            userName,
            id
        };
    }
}
