import { Injectable } from '@angular/core';
import { NewReaction } from '../models/reactions/newReaction';
import { User } from '../models/user/user';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { Reaction } from '../models/reactions/reaction';
import { CommentService } from './comment.service';
import { Comment } from '../models/comment/comment'

@Injectable({ providedIn: 'root' })
export class LikeCommentService {
    public constructor(
        private commentService: CommentService) {}

    public likeComment(comment: Comment, currentUser: User) {
        return this.setCommentReaction(comment, currentUser, true);
    }

    public dislikeComment(comment: Comment, currentUser: User) {
        return this.setCommentReaction(comment, currentUser, false);
    }

    private setCommentReaction(comment: Comment, currentUser: User, isLike: boolean){
        const innerComment = comment;

        const reaction: NewReaction = {
            entityId: innerComment.id,
            isLike: isLike,
            userId: currentUser.id
        };

        const oldUserForPostReaction = innerComment.reactions.find((x) => x.user.id === currentUser.id);

        if (oldUserForPostReaction && oldUserForPostReaction.isLike === isLike) {
            innerComment.reactions = innerComment.reactions.filter((x) => x.user.id !== currentUser.id)
        } else if (oldUserForPostReaction) {
            const newUserForPostReaction = oldUserForPostReaction;
            newUserForPostReaction.isLike = !newUserForPostReaction.isLike;
            innerComment.reactions  = innerComment.reactions
            .filter((x) => x.user.id !== currentUser.id)
            .concat(newUserForPostReaction);
        } else {
            innerComment.reactions = innerComment.reactions.concat({ isLike: isLike, user: currentUser });
        }

        return isLike
            ? this.commentService.likeComment(reaction).pipe(
                map(() => innerComment),
                catchError(() => { return this.revertChanges(innerComment, oldUserForPostReaction, isLike, currentUser); }))
            : this.commentService.dislikeComment(reaction).pipe(
                map(() => innerComment),
                catchError(() => { return this.revertChanges(innerComment, oldUserForPostReaction, isLike, currentUser); }))
    }

    revertChanges(comment: Comment, reaction: Reaction, isLike: boolean, user: User) {
 
        if (reaction && reaction.isLike === isLike) {
            comment.reactions = comment.reactions.concat({ isLike: isLike, user: user });
        } else if (reaction) {
            comment.reactions  = comment.reactions
            .filter((x) => x.user.id !== user.id)
            .concat(reaction);
        } else {
            comment.reactions = comment.reactions.filter((x) => x.user.id !== user.id);
        }

        return of(comment);
    }
}
