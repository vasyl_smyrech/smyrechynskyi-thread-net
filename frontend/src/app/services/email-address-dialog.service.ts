import { Injectable, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject, Observable } from 'rxjs';
import { EmailAddressDialogComponent } from '../components/dialogs/email-address-dialog/email-address-dialog.component';

@Injectable({ providedIn: 'root' })
export class EmailAddressDialogService implements OnDestroy {
    private unsubscribe$ = new Subject<void>();

    public constructor(private dialog: MatDialog) {}

    public openEmailAddressDialog(title: string, message: string, positiveButtonText: string) :Observable<string>{
        const dialog = this.dialog.open(EmailAddressDialogComponent, {
            data: {
                title: title,
                message: message,
                positiveButtonText: positiveButtonText
            },
            minWidth: 300,
            autoFocus: true,
            backdropClass: 'dialog-backdrop',
            position: {
                top: '0'
            }
        });

        return dialog.afterClosed()
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
