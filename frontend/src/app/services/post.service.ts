import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { NewPost } from '../models/post/new-post';
import { UpdatedPostBody } from '../models/post/updated-post-body';
import { SharePostByEmailData } from '../models/post/share-post-by-email-data';

@Injectable({ providedIn: 'root' })
export class PostService {
    public routePrefix = '/api/posts';

    constructor(private httpService: HttpInternalService) {}

    public getPosts() {
        return this.httpService.getFullRequest<Post[]>(`${this.routePrefix}`);
    }

    public createPost(post: NewPost) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}`, post);
    }

    public updatePostBody(updatedPostBody: UpdatedPostBody){
        return this.httpService.putRequest<Post>(`${this.routePrefix}/updateBody`, updatedPostBody);
    }

    public deletePost(postId: number) {
        return this.httpService.deleteRequest<Post>(`${this.routePrefix}/${postId}`)
    }

    public likePost(reaction: NewReaction) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}/like`, reaction);
    }

    public dislikePost(reaction: NewReaction) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}/dislike`, reaction);
    }

    public sharePostByEmail(sharePostByEmailData: SharePostByEmailData) {
        return this.httpService.postFullRequest(`${this.routePrefix}/sharePostByEmail`, sharePostByEmailData);
    }
}
