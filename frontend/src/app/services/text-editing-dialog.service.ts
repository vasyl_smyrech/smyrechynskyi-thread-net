import { Injectable, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject, Observable } from 'rxjs';
import { TextEditingDialogComponent } from '../components/dialogs/text-editing-dialog/text-editing-dialog.component';

@Injectable({ providedIn: 'root' })
export class TextEditingDialogService implements OnDestroy {
    private unsubscribe$ = new Subject<void>();

    public constructor(private dialog: MatDialog) {}

    public openTextEditingDialog(editedEntityName: string, bodyText: string, bodyTextMaxLength: number) :Observable<string>{
        const dialog = this.dialog.open(TextEditingDialogComponent, {
            data: {
                editedEntityName: editedEntityName,
                bodyText: bodyText,
                bodyTextMaxLength: bodyTextMaxLength
            },
            minWidth: 400,
            autoFocus: true,
            backdropClass: 'dialog-backdrop',
            position: {
                top: '0'
            }
        });

        return dialog.afterClosed()
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}