import { Component, Input, OnDestroy } from '@angular/core';
import { Post } from '../../models/post/post';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { DialogType } from '../../models/common/auth-dialog-type';
import { LikePostService } from '../../services/like-post.service';
import { NewComment } from '../../models/comment/new-comment';
import { CommentService } from '../../services/comment.service';
import { User } from '../../models/user/user';
import { Comment } from '../../models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { PostService } from 'src/app/services/post.service';
import { Observable, empty, Subject } from 'rxjs';
import { ConfirmationDialogService } from '../../services/confirmation-dialog.service';
import { TextEditingDialogService } from '../../services/text-editing-dialog.service';
import { UsersListDialogService } from 'src/app/services/users-list-dialog.service';
import { PostHubService } from 'src/app/services/post-hub.service';
import { EmailAddressDialogService } from 'src/app/services/email-address-dialog.service';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnDestroy {
    @Input() public post: Post;
    @Input() public currentUser: User;

    public showComments = false;
    public newComment = {} as NewComment;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private confirmationDialogService: ConfirmationDialogService,
        private textEditingDialogService: TextEditingDialogService,
        private usersListDialogService: UsersListDialogService,
        private emailAddressDialogService: EmailAddressDialogService, 
        private postService: PostService,
        private likePostService: LikePostService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private postHubService: PostHubService
    ) {}

    public get isCurrentUsersPost() {
        return this.currentUser ? this.post.author.id === this.currentUser.id : false;
    }

    public get likeReactionsCount() {
        return this.post.reactions.filter((x) => x.isLike).length;
    }

    public get dislikeReactionsCount() {
        return this.post.reactions.filter((x) => !x.isLike).length;
    }

    public get isDislikedByCurrentUser() {
        if (!this.currentUser) return false;
        return this.post.reactions.some(r => r.isLike === false && r.user.id === this.currentUser.id);
    }

    public get isLikedByCurrentUser() {
        if (!this.currentUser) return false;
        return this.post.reactions.some(r => r.isLike === true && r.user.id === this.currentUser.id);
    }

    public get sendLinkHref() {
        const subject = "I want share with you a post from Tread .NET";
        const formattedBody = `There is link below:\n\n\thttp://localhost:4200/?postId=${this.post.id}\n\n`;
        return `mailto:?Subject=${encodeURIComponent(subject)}&Body=${encodeURIComponent(formattedBody)}`;
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public likePost() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likePostService.likePost(this.post, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe();

            return;
        }

        this.likePostService
            .likePost(this.post, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(() => {
                if (this.post.reactions.some(r => r.user.id === this.currentUser.id && r.isLike)) {
                    const shownPostTextLength = 30;
                    this.postHubService.getPostHub.subscribe(
                    res => {
                        res.send('SendPostLiked', this.post.author.id.toString(), 
                        `<div>
                            <p>${this.currentUser.userName} liked you post starts with:
                                <br/>Text: "${this.post.body.substr(0, shownPostTextLength).trim()}"
                            </p>
                            <a href="http://localhost:4200/?postId=${this.post.id}">Go to the post</a>
                        </div>`);});}});
    }

    public dislikePost() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp: User) => this.likePostService.dislikePost(this.post, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe();

            return;
        }

        this.likePostService
            .dislikePost(this.post, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe();
    }

    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user: User) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });

            return;
        }

        this.showComments = !this.showComments;
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.newComment.body = '';
                    }
                },
                (error) => {
                    this.snackBarService.showErrorMessage("Comment creating failed");
                    console.log(error);
                });
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public openEditPostBodyDialog() {
        this.textEditingDialogService
            .openTextEditingDialog("post", this.post.body, 1000)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (updatedBody: string) => {
                    if (updatedBody) {
                        this.handlePostBodyUpdate(updatedBody);
                    }
                },
                error => this.snackBarService.showErrorMessage(error)
            );
    }

    public openDeletePostConfirmationDialog() {
        this.confirmationDialogService
            .openConfirmationDialog("Delete Post", "The post will be removed permanently!")
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                resp => {
                    if (resp) {
                        this.handlePostDelete();
                    }
                },
                error => this.snackBarService.showErrorMessage(error)
            );
    }

    public openLikedUsersListDialog() {
        this.usersListDialogService.openLikedUsersListDialog(this.post.reactions);
    }

    public openDislikedUsersListDialog() {
        this.usersListDialogService.openDislikedUsersListDialog(this.post.reactions);
    }

    public openSendPostByEmailDialog() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap(userResp => this.handleSendPostByEmail()),
                )
                .subscribe();
    

            return;
        }

        this.handleSendPostByEmail().subscribe();
    }

    private handleSendPostByEmail() {
        return this.emailAddressDialogService
        .openEmailAddressDialog("Share post by email", "Enter a person`s email who do you want to share the post with", "Share")
        .pipe(
            takeUntil(this.unsubscribe$),
            switchMap((resp: string) => {
                    if (resp) return this.postService.sharePostByEmail({postId: this.post.id, receiverEmail: resp});
                })
            )
        .pipe(
            switchMap(resp => {
                if (resp) {
                    this.snackBarService.showUsualMessage("Post has been sent");
                    return empty();
                }
            }),
            catchError((error) => {
                this.snackBarService.showErrorMessage("Post not sent");
                console.log(error)
                return empty();
            })
        );
    }

    private handlePostDelete() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe( 
                    switchMap(() => this.deletePost()),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe();

            return;
        }

            this.deletePost()
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe();
    }

    private deletePost () {
        if (this.currentUser.id === this.post.author.id) {
            return this.postService
                .deletePost(this.post.id)
                .pipe(
                    catchError((error) => { 
                        this.snackBarService.showErrorMessage("Failed during the post deleting");
                        console.log(error)
                        return empty();
                    }));
        }   
    }

    private handlePostBodyUpdate(updatedBody: string) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap(() => this.updatePostBody(updatedBody)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe();

            return;
        }

        this.updatePostBody(updatedBody)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe();
    }

    private updatePostBody(updatedBody: string) {
        if (this.currentUser.id === this.post.author.id) {
            return this.postService
                .updatePostBody({
                    postId: this.post.id,
                    body: updatedBody
                })
                .pipe(
                    catchError((error) => {
                    this.snackBarService.showErrorMessage("Failed during the post updating");
                    console.log(error);
                    return empty();
                }));       
        }
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }
}
