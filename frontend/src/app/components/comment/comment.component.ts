import { Component, Input } from '@angular/core';
import { Comment } from '../../models/comment/comment';
import { User } from 'src/app/models/user/user';
import { Observable, empty, Subject } from 'rxjs';
import { catchError } from 'rxjs/internal/operators/catchError';
import { takeUntil, switchMap, map } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/services/auth.service';
import { AuthDialogService } from 'src/app/services/auth-dialog.service';
import { ConfirmationDialogService } from 'src/app/services/confirmation-dialog.service';
import { TextEditingDialogService } from 'src/app/services/text-editing-dialog.service';
import { UsersListDialogService } from 'src/app/services/users-list-dialog.service';
import { CommentService } from 'src/app/services/comment.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { LikeCommentService } from 'src/app/services/like-comment.service';
import { DialogType } from 'src/app/models/common/auth-dialog-type';
import { Post } from 'src/app/models/post/post';
import { Console } from 'console';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent {
    @Input() public post: Post;
    @Input() public comment: Comment;
    @Input() public currentUser: User;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private confirmationDialogService: ConfirmationDialogService,
        private textEditingDialogService: TextEditingDialogService,
        private usersListDialogService: UsersListDialogService,
        private commentService: CommentService,
        private likeCommentService: LikeCommentService,
        private snackBarService: SnackBarService
    ) {}

    public get isCurrentUsersComment () {
        return this.currentUser ? this.comment.author.id === this.currentUser.id : false;
    }

    public get likeReactionsCount () {
        return this.comment.reactions.filter((x) => x.isLike).length;
    }

    public get dislikeReactionsCount () {
        return this.comment.reactions.filter((x) => !x.isLike).length;
    }

    public get isDislikedByCurrentUser () {
        if (!this.currentUser) return false;
        return this.comment.reactions.some(r => r.isLike === false && r.user.id === this.currentUser.id);
    }

    public get isLikedByCurrentUser () {
        if (!this.currentUser) return false;
        return this.comment.reactions.some(r => r.isLike === true && r.user.id === this.currentUser.id);
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public likeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp: User) => this.likeCommentService.likeComment(this.comment, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe();

            return;
        }

        this.likeCommentService
            .likeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe();
    }

    public dislikeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp: User) => this.likeCommentService.dislikeComment(this.comment, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe();

            return;
        }

        this.likeCommentService
            .dislikeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe();
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public openDeleteCommentConfirmationDialog() {
        this.confirmationDialogService
            .openConfirmationDialog("Delete Comment", "The comment will be removed permanently!")
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                resp => {
                    if (resp) this.handleCommentDelete();
                },
                error => this.snackBarService.showErrorMessage(error)
            );
    }

    public openEditCommentBodyDialog() {
        this.textEditingDialogService
            .openTextEditingDialog("comment", this.comment.body, 500)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (updatedBody: string) => {
                    if (updatedBody) this.handleCommentBodyUpdate(updatedBody);
                },
                error => this.snackBarService.showErrorMessage(error)
            );
    }

    public openDislikedUsersListDialog() {
        this.usersListDialogService.openDislikedUsersListDialog(this.comment.reactions);
    }

    public openLikedUsersListDialog() {
        this.usersListDialogService.openLikedUsersListDialog(this.comment.reactions);
    }

    private handleCommentDelete() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe( 
                    switchMap(() => this.deleteComment()),
                    takeUntil(this.unsubscribe$)
                    )
                .subscribe();

            return;
        }

            this.deleteComment()
                .subscribe();
    }

    private deleteComment () {
        if (this.currentUser.id === this.comment.author.id) {
            return this.commentService
                .deleteComment(this.comment.id)
                .pipe(
                    catchError((error) => { 
                        this.snackBarService.showErrorMessage("Failed during the comment deleting");
                        console.log(error)
                        return empty();
                    }));
        }   
    }

    private handleCommentBodyUpdate(updatedBody: string) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap(() => this.updateCommentBody(updatedBody)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe();

            return;
        }

        this.updateCommentBody(updatedBody)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe();
    }

    private updateCommentBody(updatedBody: string) {
        if (this.currentUser.id === this.comment.author.id) {
            return this.commentService
                .updateCommentBody({
                    commentId: this.comment.id,
                    body: updatedBody
                })
                .pipe(
                    catchError((error) => {
                        this.snackBarService.showErrorMessage("Failed during the comment updating");
                        console.log(error)
                        return empty();
                    }));       
        }
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }
}
