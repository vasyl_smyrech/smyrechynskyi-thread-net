import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from '../../models/user/user';
import { Subject } from 'rxjs';
import { UserService } from '../../services/user.service';
import { AuthenticationService } from '../../services/auth.service';
import { takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.sass']
})
export class ResetPasswordComponent implements OnInit, OnDestroy {
    public user = {} as User;
    public hidePass = true;
    public showError = false;
    public showResetPasswordForm = false;
    public email: string;
    public resetPassword: string; 
    public confirmResetPassword: string;
    public passwordPattern = new RegExp("^[a-zA-Z0-9[-_/ !@#$%^&*()<>{},.+=-]+$"); 

    private unsubscribe$ = new Subject<void>();
    private userIdFromRequest: number;
    private hashFromRequest: string;

    constructor(
      private userService: UserService,
      private snackBarService: SnackBarService,
      private authService: AuthenticationService,
      private route: ActivatedRoute,
      private router: Router
    ) {}

    public ngOnInit() {
      this.route.queryParams.subscribe((params: Params) => {
        this.userIdFromRequest = parseInt(params['user']);
        this.hashFromRequest = encodeURI(params['hash']);

        if (this.userIdFromRequest && this.hashFromRequest){
          this.userService.approveResetPasswordRequest({userId: this.userIdFromRequest, hash: this.hashFromRequest})
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe(
            () => {this.showResetPasswordForm = true},
            () => {this.showError = true;}
            );
        } else {
          this.showError = true;
        }
      });
    }

    public saveNewPassword() {
      this.userService.updateUserPassword({
        userId: this.userIdFromRequest, 
        newPassword: this.resetPassword,
        hash: this.hashFromRequest})
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          (resp) => {
              this.snackBarService.showUsualMessage('Password successfully updated');
              this.authService.login({email: resp.body.email, password: this.resetPassword})
                .subscribe((result: User) => {
                  if (result) {
                      this.authService.setUser(result);
                      this.router.navigate(['../'], { relativeTo: this.route });
                  }
              });
          },
          (error) => {
            this.snackBarService.showErrorMessage("New password not saved");
            console.log(error);
          });
  }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
