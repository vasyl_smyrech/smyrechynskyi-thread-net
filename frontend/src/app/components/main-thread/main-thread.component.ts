import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Post } from '../../models/post/post';
import { User } from '../../models/user/user';
import { Subject } from 'rxjs';
import { AuthenticationService } from '../../services/auth.service';
import { PostService } from '../../services/post.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { DialogType } from '../../models/common/auth-dialog-type';
import { EventService } from '../../services/event.service';
import { ImgurService } from '../../services/imgur.service';
import { NewPost } from '../../models/post/new-post';
import { switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { PostFilterType } from 'src/app/models/common/post-filter-type';
import { ToastrService } from 'ngx-toastr';
import { Comment } from 'src/app/models/comment/comment';
import { PostHubService } from 'src/app/services/post-hub.service';
import { Params, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-main-thread',
    templateUrl: './main-thread.component.html',
    styleUrls: ['./main-thread.component.sass']
})
export class MainThreadComponent implements OnInit, OnDestroy {
    public posts: Post[] = [];
    public cachedPosts: Post[] = [];
    public currentUser: User;
    public imageUrl: string;
    public imageFile: File;
    public post = {} as NewPost;
    public showPostContainer = false;
    public loading = false;
    public loadingPosts = false;
    public shouldImageErrorBeActive = false;
    public throttle = 300;
    public scrollDistance = 1;
    public scrollUpDistance = 2;
    public imageInputValidationError = "Image required *"
    public bodyTextPattern = new RegExp(".*[^ |\n|\t].*");
    public currentPostFilter = window.localStorage.getItem("currentPostFilter") && this.currentUser
        ? PostFilterType[window.localStorage.getItem("currentPostFilter")] 
        : PostFilterType.All;
    public postFilterMenuItems = [
        {
            name: "All",
            filterType: PostFilterType.All,
        },
        {
            name: "Only mine",
            filterType: PostFilterType.OnlyMine,
        },
        {
            name: "Without mine",
            filterType: PostFilterType.WithoutMine,
        },
        {
            name: "Liked by me",
            filterType: PostFilterType.LikedByMe,
        }
    ];

    private unsubscribe$ = new Subject<void>();
    private firstPostIdIfSet: number; 
    private postsCount: number;

    @ViewChild('newPostForm') newPostForm: HTMLFormElement;
    @ViewChild('file') choseImageInput: ElementRef;
  

    public constructor(
        private route: ActivatedRoute,
        private snackBarService: SnackBarService,
        private authService: AuthenticationService,
        private postService: PostService,
        private imgurService: ImgurService,
        private authDialogService: AuthDialogService,
        private eventService: EventService,
        private toastrService: ToastrService,
        private postHubService: PostHubService
    ) {}

    public get currentPostFilterName() {
        return this.postFilterMenuItems
        .find(i => i.filterType === this.currentPostFilter)
        .name;
    }

    public ngOnInit() {  
        this.route.queryParams.subscribe((params: Params) => {
            this.firstPostIdIfSet = params['postId'];
            this.getPosts();
            this.getUser()
             .subscribe(
                (user) => {
                    this.currentUser = user;
                    this.registerHubListeners();
                },
                () =>  this.registerHubListeners());
    
            this.eventService.userChangedEvent$
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe(user => {
                    this.currentUser = user;
                    this.post.authorId = this.currentUser ? this.currentUser.id : undefined;
                }
            );
        });
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
        this.postHubService.getPostHub.subscribe(res => res.stop());
    }
  
    public getPosts() {
        this.loadingPosts = true;
        this.postService
            .getPosts()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    this.loadingPosts = false;
                    this.cachedPosts = resp.body;
                    this.filterPosts(this.currentPostFilter);
                },
                () => this.loadingPosts = false
            );
    }

    public sendPost() {
        this.loading = true;

        this.imgurService
            .uploadToImgur(this.imageFile, 'title')
            .pipe(
                switchMap((imageData) => {
                    this.post.previewImage = imageData.body.data.link;
                    return this.postService.createPost(this.post);
                })
              )
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(() => {
                this.removeImage();
                this.post.body = undefined;
                this.post.previewImage = undefined;
                this.loading = false;
                this.showPostContainer = false;
            },
            (error) => {
                this.loading = false;
                this.snackBarService.showErrorMessage(error)
            }
        );
    }

    public loadImage(target: HTMLInputElement) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            this.shouldImageErrorBeActive = true;
            this.imageInputValidationError = "Image required *"
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.shouldImageErrorBeActive = true;
            this.imageInputValidationError = "Image can't be heavier than ~5MB"
            return;
        }

        this.shouldImageErrorBeActive = false;
        this.imageInputValidationError = null

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.imageUrl = reader.result as string));
        reader.readAsDataURL(this.imageFile);
    }

    public removeImage() {
        this.shouldImageErrorBeActive = true;
        this.imageInputValidationError = "Image required *"
        this.imageUrl = undefined;
        this.imageFile = undefined;
        this.choseImageInput.nativeElement.value = null;
    }

    public filterPosts(filterType: PostFilterType) {
        if (this.firstPostIdIfSet) {
            filterType = PostFilterType.All;
            this.cachedPosts
                .unshift(this.cachedPosts
                    .splice(this.cachedPosts
                        .findIndex(p => p.id == this.firstPostIdIfSet), 1)[0]);

            this.firstPostIdIfSet = null;
        }


        switch(filterType){
            case PostFilterType.All:
                this.posts = this.cachedPosts;
                break;
            case PostFilterType.OnlyMine:
                this.posts = this.cachedPosts
                    .filter(p => p.author.id === this.currentUser.id);
                break;
            case PostFilterType.WithoutMine:
                this.posts = this.cachedPosts
                    .filter(p => p.author.id !== this.currentUser.id);
                break;
            case PostFilterType.LikedByMe:
                this.posts = this.cachedPosts
                    .filter(p => p.reactions
                    .some(r => r.user.id === this.currentUser.id && r.isLike));
                break;
        }

        this.postsCount = this.posts.length;

        this.currentPostFilter = filterType;
        window.localStorage.setItem("currentPostFilter", PostFilterType[filterType]);
    }

    public toggleNewPostContainer() {
        this.showPostContainer = !this.showPostContainer;
        if (this.showPostContainer) this.shouldImageErrorBeActive = false;
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public onScrollDown () {
        console.log('scrolled down!!');
    
        // add another 10 posts
         const start = this.postsCount;
         this.postsCount += 10;
         this.appendPosts(start);
    }

    private appendPosts(start: number) {
        for (let i = start; i < this.postsCount; ++i) {
            this.posts.push(this.posts[Math.floor(Math.random() * this.posts.length)]);
          }
    }

    private registerHubListeners() {
        this.postHubService.getPostHub.subscribe(
            (resp) => resp.on('PostLiked', (message: string) => {
            this.toastrService.show(
                message,
                null, 
                {
                    toastClass: "toast-message-container",
                    enableHtml: true,
                    closeButton: true,
                    timeOut: 100000
                });
            }));

        this.postHubService.getPostHub.subscribe(
            (resp) => resp.on('NewPost', (newPost: Post) => {
            if (newPost) {
                this.onNewPostAdded(newPost);
            }}));

        this.postHubService.getPostHub.subscribe(
            (resp) => resp.on('PostDeleted', (postId: number) => {
            if (postId) {
                this.onPostDeleted(postId);
            }}));

        this.postHubService.getPostHub.subscribe(
            (resp) => resp.on('PostBodyModified', (data) => {
            if (data.postId && data.postBody ) {
                this.onPostBodyModified(data.postId, data.postBody);
            }}));

        this.postHubService.getPostHub.subscribe(
            (resp) => resp.on('PostReactionChanged', (data) => {
            if (data.entityId && data.userId ) {
                this.onPostReactionsChanged(data);
            }}));

        this.postHubService.getPostHub.subscribe(
            (resp) => resp.on('NewComment', (newComment: Comment) => {
            if (newComment) {
                this.onNewCommentAdded(newComment);
            }}));

        this.postHubService.getPostHub.subscribe(
            (resp) => resp.on('CommentDeleted', (commentId: number) => {
            if (commentId) {
                this.onCommentDeleted(commentId);
            }}));

        this.postHubService.getPostHub.subscribe(
            (resp) => resp.on('CommentBodyModified', (data) => {
            if (data.commentId && data.commentBody ) {
                this.onCommentBodyModified(data.commentId, data.commentBody);
            }}));

        this.postHubService.getPostHub.subscribe(
            (resp) => resp.on('CommentReactionChanged', (data) => {
            if (data.entityId && data.userId ) {
                this.onCommentReactionsChanged(data);
            }}));
    }

    private onPostReactionsChanged(data: any) {
        const post = this.posts.find(p => p.id === data.entityId);

        if (post && data.deleted) {
            post.reactions = post.reactions.filter(r => r.user.id !== data.userId)
        } else if (post) {
            let postReaction = post.reactions.find(r => r.user.id === data.userId);

            if (postReaction) {
                postReaction.isLike = data.isLike;
            } else {
                post.reactions.push({
                    isLike: data.isLike, 
                    user: {
                        id: data.userId, 
                        email: data.userEmail,
                        userName: data.userName,
                        avatar: data.userAvatar
                    }
                });
            }
        }  
    }

    private onPostBodyModified(postId: number, postBody: string) {
        let modifiedCachedPost = this.cachedPosts.find(p => p.id === postId);
        if (modifiedCachedPost) modifiedCachedPost.body = postBody;
    }

    private onPostDeleted(deletedPostId: number) {
        const deletedPost = this.cachedPosts.find(p => p.id === deletedPostId);
        if (deletedPost && deletedPost.author.id === this.currentUser.id) {
            this.toastrService.success('The post has been deleted!');
        }

        this.cachedPosts = this.cachedPosts.filter(p => p.id !== deletedPostId);
        this.posts = this.posts.filter(p => p.id !== deletedPostId);
    }

    private onNewPostAdded(newPost: Post) {
        if (!this.cachedPosts.some((x) => x.id === newPost.id)) {
            let toastMessage = 'Change the posts filter for it\'s observing!';

            this.cachedPosts = this.sortPostArray(this.cachedPosts.concat(newPost));

            if (this.currentPostFilter === PostFilterType.All 
                || (this.currentPostFilter === PostFilterType.OnlyMine && newPost.author.id === this.currentUser.id)) {
                    this.posts = this.sortPostArray(this.posts.concat(newPost));
                    toastMessage = '';
            }

            if (newPost.author.id === this.currentUser.id) {
                this.toastrService.success(toastMessage, 'Post created!');
            }
        }
    }

    private onCommentReactionsChanged(data: any) {
        const post = this.posts.find(p => p.comments.some(c => c.id === data.entityId));
        if (post) {
            let comment = post.comments.find(c => c.id === data.entityId);

            if (comment && data.deleted) {
                comment.reactions = comment.reactions.filter(r => r.user.id !== data.userId)
            } else if (comment) {
                let commentReaction = comment.reactions.find(r => r.user.id === data.userId);
    
                if (commentReaction) {
                    commentReaction.isLike = data.isLike;
                } else {
                    comment.reactions.push({
                        isLike: data.isLike, 
                        user: {
                            id: data.userId, 
                            email: data.userEmail,
                            userName: data.userName,
                            avatar: data.userAvatar
                        }
                    });
                }
            }
        }   
    }

    private onCommentBodyModified(commentId: number, body: string) {
        let post = this.posts.find(p => p.comments.some(c => c.id === commentId));
        if (post) post.comments.find(c => c.id === commentId).body = body;
    }

    private onCommentDeleted(deletedCommentId: number) {
        let post = this.posts.find(p => p.comments.some(c => c.id === deletedCommentId));
        if (post) post.comments = post.comments.filter(c => c.id !== deletedCommentId);
    }

    private onNewCommentAdded(data: any) {
        var parentPost = this.cachedPosts.find((p) => p.id === data.parentPostId);
        if (parentPost) parentPost.comments.push(data.createdComment);
    }

    private getUser() {
        return this.authService
            .getUser()
            .pipe(takeUntil(this.unsubscribe$));
    }

    private sortPostArray(array: Post[]): Post[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }
}
