import { Component, Inject, OnDestroy, ElementRef, AfterViewInit, Renderer2 } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { Reaction } from 'src/app/models/reactions/reaction';

@Component({
  selector: 'app-users-list-dialog',
  templateUrl: './users-list-dialog.component.html',
  styleUrls: ['./users-list-dialog.component.sass']
})

export class UsersListDialogComponent implements AfterViewInit, OnDestroy {
  public title: string;
  public reactions: Reaction[];
  private unsubscribe$ = new Subject<void>();

  constructor(
    private dialogRef: MatDialogRef<UsersListDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private elementRef: ElementRef,
    private renderer: Renderer2
  ) {
    this.title = data.title,
    this.reactions = data.reactions
  }

  //sets mat-list-item padding: 0
  public ngAfterViewInit(): void {
    const listItems= this.elementRef.nativeElement.querySelectorAll('.mat-list-item-content') as HTMLElement[];
    listItems.forEach(listItem=> {
        this.renderer.setStyle(listItem, 'padding', '0px');
    });
}

  public ngOnDestroy() {
      this.unsubscribe$.next();
      this.unsubscribe$.complete();
  }

  public close() {
      this.dialogRef.close();
  }
}