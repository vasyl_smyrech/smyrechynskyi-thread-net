import { Component, Inject, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-text-editing-dialog',
  templateUrl: './text-editing-dialog.component.html',
  styleUrls: ['./text-editing-dialog.component.sass']
})
export class TextEditingDialogComponent implements OnDestroy {
  public editedEntityName: string;
  public bodyText: string;
  public positiveButtonText: string;
  public negativeButtonText: string;
  public bodyTextMaxLength: number;
  public bodyTextPattern = new RegExp(".*[^ |\n|\t].*");

  private unsubscribe$ = new Subject<void>();

  constructor(
    private dialogRef: MatDialogRef<TextEditingDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.editedEntityName = this.capitalize(data.editedEntityName),
    this.bodyText = data.bodyText,
    this.positiveButtonText = data.positiveButtonText??"Confirm",
    this.negativeButtonText = data.negativeButtonText??"Cancel",
    this.bodyTextMaxLength = data.bodyTextMaxLength??1000
  }

  public ngOnDestroy() {
      this.unsubscribe$.next();
      this.unsubscribe$.complete();
  }

  public close() {
      this.dialogRef.close();
  }

  public onConfirm() {
    this.dialogRef.close(this.bodyText.trim());
  }

  private capitalize(word: string) {
    return word.charAt(0).toUpperCase() + word.toLowerCase().slice(1)
  }
}
  
  