import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogType } from '../../../models/common/auth-dialog-type';
import { Subject } from 'rxjs';
import { AuthenticationService } from '../../../services/auth.service';
import { takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../../services/snack-bar.service';
import { UserService } from 'src/app/services/user.service';

@Component({
    templateUrl: './auth-dialog.component.html',
    styleUrls: ['./auth-dialog.component.sass']
})
export class AuthDialogComponent implements OnInit, OnDestroy {
    public dialogType = DialogType;
    public userName: string;
    public password: string;
    public avatar: string;
    public email: string;
    public hidePass = true;
    public title: string;
    public usernamePattern = new RegExp("^[a-zA-Z0-9 ]+$");
    public passwordPattern = new RegExp("^[a-zA-Z0-9[-_/ !@#$%^&*()<>{},.+=-]+$"); 

    private unsubscribe$ = new Subject<void>();

    constructor(
        private dialogRef: MatDialogRef<AuthDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private authService: AuthenticationService,
        private userService: UserService,
        private snackBarService: SnackBarService
    ) {}

    public ngOnInit() {
        this.avatar = 'https://i.imgur.com/KXOxirM.jpg';
        this.setTitle();
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public close() {
        this.dialogRef.close(false);
    }

    public doPositiveButtonAction() {
        switch(this.data.dialogType){
            case DialogType.SignIn:
                this.signIn();
                break;
            case DialogType.SignUp:
                this.signUp();
                break;
            case DialogType.ResetPassword:
                this.sendResetPasswordEmail();
                break;
        }
    }

    public changeToResetPasswordForm() {
        this.data.dialogType = DialogType.ResetPassword;
        this.setTitle();
    }

    private signIn() {
        this.authService
            .login({ email: this.email, password: this.password })
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (response) => this.dialogRef.close(response), 
                (error) => {
                    this.snackBarService.showErrorMessage("Wrong password or email");
                    console.log(error);
                });
    }

    private signUp() {
        this.authService
            .register({ userName: this.userName, password: this.password, email: this.email, avatar: this.avatar })
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (response) => this.dialogRef.close(response),
                (error) => {
                    this.snackBarService.showErrorMessage("SignUp Failed");
                    console.log(error);
                });
    }

    private sendResetPasswordEmail() {
        this.userService
            .sendResetPasswordEmail({resetEmail: this.email})
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (response) => {
                    this.dialogRef.close(null);
                    this.snackBarService.showUsualMessage(`Email with a reset password instruction has been sent to ${response.body.resetEmail}`)
                }, 
                (error) => {
                    this.snackBarService.showErrorMessage(error);
                    console.log(error)
                });
    }

    private setTitle() {
        switch(this.data.dialogType){
            case DialogType.SignIn:
                this.title = 'Sign In';
                break;
            case DialogType.SignUp:
                this.title = 'Sign Up';
                break;
            case DialogType.ResetPassword:
                this.title = 'Reset Password'
                break;
        }
    }
}
