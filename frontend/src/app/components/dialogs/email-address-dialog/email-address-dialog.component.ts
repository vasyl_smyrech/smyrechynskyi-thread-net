import { Component, Inject } from '@angular/core';
import { Subject } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-email-address-dialog',
  templateUrl: './email-address-dialog.component.html',
  styleUrls: ['./email-address-dialog.component.sass']
})
export class EmailAddressDialogComponent {
  public positiveButtonText: string;
  public receiverEmail: string;
  public negativeButtonText: string;
  public title: string;
  public message: string;
  
  private unsubscribe$ = new Subject<void>();

  constructor(
    private dialogRef: MatDialogRef<EmailAddressDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.title = data.title,
    this.message = data.message,
    this.positiveButtonText = data.positiveButtonText??"Confirm",
    this.negativeButtonText = data.negativeButtonText??"Cancel"
  }

  public ngOnDestroy() {
      this.unsubscribe$.next();
      this.unsubscribe$.complete();
  }

  public close() {
      this.dialogRef.close();
  }

  public onConfirm() {
      this.dialogRef.close(this.receiverEmail);
  }
}
  
  