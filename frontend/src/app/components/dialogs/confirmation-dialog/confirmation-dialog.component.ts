import { Component, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.sass']
})
export class ConfirmationDialogComponent implements OnDestroy {
  public hidePass = true;
  public title: string;
  public message: string;
  public positiveButtonText: string;
  public negativeButtonText: string;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.title = data.title,
    this.message = data.message,
    this.positiveButtonText = data.positiveButtonText??"Confirm",
    this.negativeButtonText = data.negativeButtonText??"Cancel"
}

  public ngOnDestroy() {
      this.unsubscribe$.next();
      this.unsubscribe$.complete();
  }

  public close() {
      this.dialogRef.close(false);
  }

  public onConfirm() {
    this.dialogRef.close(true);
  }
}

