export interface SharePostByEmailData {
    postId: number;
    receiverEmail: string;
}