export interface UpdatedPostBody {
    postId: number;
    body: string;
}