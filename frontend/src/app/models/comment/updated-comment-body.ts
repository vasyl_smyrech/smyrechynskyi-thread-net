export interface UpdatedCommentBody {
    commentId: number;
    body: string;
}