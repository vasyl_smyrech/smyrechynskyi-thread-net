export interface ApproveResetPasswordRequestDto {
    userId: number;
    hash: string;
}