export interface UpdateUserPasswordDto {
    userId: number;
    newPassword: string;
    hash: string;
}