export interface ResetPasswordEmailDto {
    resetEmail: string;
}
