import { User } from '../user/user';

export interface Reaction {
    isLike: boolean;
    user: User;
}
