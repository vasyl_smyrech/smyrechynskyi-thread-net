export enum PostFilterType {
    All = 0,
    OnlyMine,
    WithoutMine,
    LikedByMe
}
