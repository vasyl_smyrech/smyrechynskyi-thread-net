﻿using Newtonsoft.Json;

namespace Thread_.NET.Common.DTO.Comment
{
    public sealed class CommentUpdateBodyDTO
    {
        public int CommentId { get; set; }

        public string Body { get; set; }
    }
}
