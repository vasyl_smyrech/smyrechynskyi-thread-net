﻿using Newtonsoft.Json;

namespace Thread_.NET.Common.DTO.Post
{
    public sealed class PostUpdateBodyDTO
    {
        [JsonIgnore]
        public int PostId { get; set; }

        public string Body { get; set; }
    }
}
