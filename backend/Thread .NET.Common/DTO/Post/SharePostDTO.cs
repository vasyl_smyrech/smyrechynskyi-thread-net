﻿namespace Thread_.NET.Common.DTO.Post
{
    public sealed class SharePostDTO
    {
        public int PostId { get; set; }
        public string ReceiverEmail { get; set; }
    }
}
