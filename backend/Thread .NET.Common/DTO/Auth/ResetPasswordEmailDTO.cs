﻿namespace Thread_.NET.Common.DTO.Auth
{
    public sealed class ResetPasswordEmailDTO
    {
        public string ResetEmail { get; set; }
    }
}
