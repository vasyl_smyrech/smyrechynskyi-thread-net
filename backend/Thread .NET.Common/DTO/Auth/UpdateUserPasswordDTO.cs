﻿namespace Thread_.NET.Common.DTO.Auth
{
    public sealed class UpdateUserPasswordDTO
    {
        public int UserId { get; set; }
        public string NewPassword { get; set; }
        public string Hash { get; set; }
    }
}
