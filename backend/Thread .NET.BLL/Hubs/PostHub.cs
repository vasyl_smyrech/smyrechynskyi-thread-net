﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace Thread_.NET.BLL.Hubs
{
    public sealed class PostHub : Hub
    {
        public Task SendPostLiked(string userId, string message)
        {
            return Clients.Group(userId).SendAsync("PostLiked", message);
        }

        public Task MapUserToConnection(string userId)
        {
            return Groups.AddToGroupAsync(Context.ConnectionId, userId);
        }

        public Task ExcludeUserFromConnections(string userId)
        {
            return Groups.RemoveFromGroupAsync(Context.ConnectionId, userId);
        }
    }
}
