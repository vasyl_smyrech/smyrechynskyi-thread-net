﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.DAL.Context;
using Thread_.NET.ReusableUI.Services;
using Thread_.NET.Common.DTO.User;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.ReusableUI.Areas.Emails.SharedViewModels;

namespace Thread_.NET.BLL.Services
{
    public sealed class LikeService : BaseService
    {
        private readonly IHubContext<PostHub> _postHubContext;
        private readonly MailService _mailService;
        private readonly IRazorViewToStringRenderer _razorViewToStringRenderer;

        public LikeService(
            ThreadContext context, 
            IMapper mapper, 
            IHubContext<PostHub> postHubContext,
            IRazorViewToStringRenderer razorViewToStringRenderer,
            MailService mailService) : base(context, mapper) 
        {
            _postHubContext = postHubContext;
            _mailService = mailService;
            _razorViewToStringRenderer = razorViewToStringRenderer;
        }

        public async Task SetPostReaction(NewReactionDTO reaction)
        {
            var oldReaction = _context.PostReactions
                .Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId)
                .FirstOrDefault();

            var deleted = false;

            if (oldReaction != null && oldReaction.IsLike == reaction.IsLike)
            {
                _context.PostReactions.Remove(oldReaction);
                deleted = true;
            }
            else if (oldReaction != null)
            {
                oldReaction.IsLike = !oldReaction.IsLike;
            }
            else
            {
                _context.PostReactions.Add(new DAL.Entities.PostReaction
                {
                    PostId = reaction.EntityId,
                    IsLike = reaction.IsLike,
                    UserId = reaction.UserId
                });
            }

            await _context.SaveChangesAsync();

            await SendReactions(reaction, deleted, "PostReactionChanged");

            if (reaction.IsLike)
            {
                var post = await _context.Posts
                    .Include(p => p.Author)
                        .ThenInclude(a => a.Avatar)
                    .Include(p => p.Preview)
                    .FirstOrDefaultAsync(p => p.Id == reaction.EntityId);

                var user = _context.Users
                    .Include(u => u.Avatar)
                    .Where(u => u.Id == reaction.UserId)
                    .FirstOrDefault();

                if (post != null && user != null)
                {
                    var userName = user.UserName;
                    var userAvatarUrl = user.Avatar?.URL;

                    var mailSubject = $"{userName.Substring(0, 1).ToUpper() + userName.Substring(1)} liked your post";
                    var userLikedPostVM = new PostEmailViewModel(_mapper.Map<UserDTO>(user), _mapper.Map<PostDTO>(post), userAvatarUrl);

                    var body = await _razorViewToStringRenderer.RenderViewToStringAsync("/Views/Emails/PostLiked/PostLikedEmail.cshtml", userLikedPostVM);

                    _mailService.SendEmail(post.Author.Email, mailSubject, body, true);
                }
            }
        }

        public async Task SetCommentReaction(NewReactionDTO reaction)
        {
            var oldReaction = _context.CommentReactions
                .Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId)
                .FirstOrDefault();

            var deleted = false;

            if (oldReaction != null && oldReaction.IsLike == reaction.IsLike)
            {
                _context.CommentReactions.Remove(oldReaction);
                deleted = true;
            }
            else if (oldReaction != null)
            {
                oldReaction.IsLike = !oldReaction.IsLike;
            }
            else
            {
                _context.CommentReactions.Add(new DAL.Entities.CommentReaction
                {
                    CommentId = reaction.EntityId,
                    IsLike = reaction.IsLike,
                    UserId = reaction.UserId
                });
            }

            await _context.SaveChangesAsync();

            await SendReactions(reaction, deleted, "CommentReactionChanged");
        }

        private async Task SendReactions(NewReactionDTO reaction, bool deleted, string method)
        {
            var user = _context.Users
             .Include(user => user.Avatar)
             .Where(x => x.Id == reaction.UserId)
             .FirstOrDefault();

            var data = new
            {
                entityId = reaction.EntityId,
                isLike = reaction.IsLike,
                userId = reaction.UserId,
                userName = user?.UserName,
                userEmail = user?.Email,
                userAvatar = user?.Avatar?.URL,
                deleted
            };

            await _postHubContext.Clients.All.SendAsync(method, data);
        }
    }
}
