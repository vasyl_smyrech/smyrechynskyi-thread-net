﻿using Microsoft.Extensions.Configuration;
using System;
using System.Net.Mail;

namespace Thread_.NET.BLL.Services
{
    public sealed class MailService
    {
        private readonly IConfiguration _configuration;

        public MailService(IConfiguration configuration) 
        {
            _configuration = configuration;
        }

        public void SendEmail(string receivers, string mailSubject, string body, bool isBodyHtml)
        {
            MailMessage message = new MailMessage
            {
                From = new MailAddress($"smyrechynskyi.thread@gmail.com", "Thread.Net"),
                Subject = mailSubject,
                IsBodyHtml = isBodyHtml,
                Body = body
            };
            message.To.Add(receivers);

            using var smtpClient = new SmtpClient
            {
                EnableSsl = true,
                UseDefaultCredentials = true,
                Host = _configuration["smtpData:hostHost"],
                Port = Int32.Parse(_configuration["smtpData:hostPort"]),
                Credentials = new System.Net.NetworkCredential(_configuration["smtpData:hostEmail"], _configuration["smtpData:hostPassword"])
            };

            smtpClient.Send(message);
        }
    }
}
