﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Auth;
using Thread_.NET.Common.DTO.User;
using Thread_.NET.Common.Security;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;
using Thread_.NET.ReusableUI.Areas.Emails.ResetPassword;
using Thread_.NET.ReusableUI.Services;

namespace Thread_.NET.BLL.Services
{
    public sealed class UserService : BaseService
    {
        private readonly MailService _mailService;
        private readonly IRazorViewToStringRenderer _razorViewToStringRenderer;

        public UserService(
            ThreadContext context, 
            IMapper mapper,
            IRazorViewToStringRenderer razorViewToStringRenderer,
            MailService mailService) : base(context, mapper) 
        {
            _mailService = mailService;
            _razorViewToStringRenderer = razorViewToStringRenderer;
        }

        public async Task<ICollection<UserDTO>> GetUsers()
        {
            var users = await _context.Users
                .Include(x => x.Avatar)
                .ToListAsync();

            return _mapper.Map<ICollection<UserDTO>>(users);
        }

        public async Task<UserDTO> GetUserById(int id)
        {
            var user = await GetUserByIdInternal(id);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), id);
            }

            return _mapper.Map<UserDTO>(user);
        }

        public async Task<UserDTO> CreateUser(UserRegisterDTO userDto)
        {
            var userEntity = _mapper.Map<User>(userDto);
            var salt = SecurityHelper.GetRandomBytes();

            userEntity.Salt = Convert.ToBase64String(salt);
            userEntity.Password = SecurityHelper.HashPassword(userDto.Password, salt);

            _context.Users.Add(userEntity);
            await _context.SaveChangesAsync();

            return _mapper.Map<UserDTO>(userEntity);
        }

        public async Task UpdateUser(UserDTO userDto)
        {
            var userEntity = await GetUserByIdInternal(userDto.Id);
            if (userEntity == null)
            {
                throw new NotFoundException(nameof(User), userDto.Id);
            }

            var timeNow = DateTime.Now;

            userEntity.Email = userDto.Email;
            userEntity.UserName = userDto.UserName;
            userEntity.UpdatedAt = timeNow;

            if (!string.IsNullOrEmpty(userDto.Avatar))
            {
                if (userEntity.Avatar == null)
                {
                    userEntity.Avatar = new Image
                    {
                        URL = userDto.Avatar
                    };
                }
                else
                {
                    userEntity.Avatar.URL = userDto.Avatar;
                    userEntity.Avatar.UpdatedAt = timeNow;
                }
            }
            else
            {
                if (userEntity.Avatar != null)
                {
                    _context.Images.Remove(userEntity.Avatar);
                }
            }

            _context.Users.Update(userEntity);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteUser(int userId)
        {
            var userEntity = await _context.Users.FirstOrDefaultAsync(u => u.Id == userId);

            if (userEntity == null)
            {
                throw new NotFoundException(nameof(User), userId);
            }

            _context.Users.Remove(userEntity);
            await _context.SaveChangesAsync();
        }

        public async Task SendResetUserPasswordEmail(ResetPasswordEmailDTO dto)
        {
            var user = _context.Users.FirstOrDefault(u => u.Email == dto.ResetEmail);

            if (user == null)
                throw new NotFoundException($"There is no user with {dto.ResetEmail} email");
            var hash = SecurityHelper.HashPassword(user.Password, Convert.FromBase64String(user.Salt));
            var hashBytes = System.Text.Encoding.UTF8.GetBytes(hash);
            var hashEncodedBase64 = Convert.ToBase64String(hashBytes);
            hashEncodedBase64.Replace("+", "[");
            hashEncodedBase64.Replace("/", "]");
            var resetPasswordUrl = $"http://localhost:4200/resetpassword/?user={user.Id}&hash={hashEncodedBase64}";
            var mailSubject = "Reset password on Thread .NET";
            var userLikedPostVM = new ResetPasswordEmailViewModel(_mapper.Map<UserDTO>(user), resetPasswordUrl);
            var body = await _razorViewToStringRenderer.RenderViewToStringAsync("/Views/Emails/ResetPassword/ResetPasswordEmail.cshtml", userLikedPostVM);

            _mailService.SendEmail(dto.ResetEmail, mailSubject, body, true);
        }

        public async Task<User> ApproveResetUserPasswordRequest(int userId, string hash)
        {
            var user = await _context.Users
               .FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null)
            {
                throw new NotFoundException("There is no such user");
            }

            var base64EncodedBytes = Convert.FromBase64String(hash);
            var decodedHash = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            decodedHash.Replace("[", "+");
            decodedHash.Replace("]", "/");
            var trueHash = SecurityHelper.HashPassword(user.Password, Convert.FromBase64String(user.Salt));

            if (trueHash != decodedHash)
            {
                throw new FieldAccessException("Wrong data");
            }

            return user;
        }

        public async Task<User> UpdateUserPassword(UpdateUserPasswordDTO dto)
        {
            var user = await ApproveResetUserPasswordRequest(dto.UserId, dto.Hash);

            user.Password = SecurityHelper.HashPassword(dto.NewPassword, Convert.FromBase64String(user.Salt));

            await _context.SaveChangesAsync();

            return user;
        }

        private async Task<User> GetUserByIdInternal(int id)
        {
            return await _context.Users
                .Include(u => u.Avatar)
                .FirstOrDefaultAsync(u => u.Id == id);
        }
    }
}
