﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        private readonly IHubContext<PostHub> _postHubContext;

        public CommentService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHubContext) : base(context, mapper) 
        {
            _postHubContext = postHubContext;
        }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);
            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            var createdCommentDTO = _mapper.Map<CommentDTO>(createdComment);
            await _postHubContext.Clients.All.SendAsync("NewComment",
                new { parentPostId = newComment.PostId, createdComment = createdCommentDTO });

            return createdCommentDTO;
        }

        public async Task<CommentDTO> UpdateCommentBody(CommentUpdateBodyDTO updateBodyDTO, int currentUserId)
        {
            var comment = await _context.Comments
            .Include(comment => comment.Author)
                .ThenInclude(author => author.Avatar)
            .FirstOrDefaultAsync(p => p.Id == updateBodyDTO.CommentId);

            if (comment != null)
            {
                if (currentUserId != comment.AuthorId)
                    throw new AccessViolationException($"User with Id = {currentUserId} attempted to update other user's comment");

                comment.Body = updateBodyDTO.Body;
                await _context.SaveChangesAsync();
                await _postHubContext.Clients.All.SendAsync("CommentBodyModified",
                    new { commentId = comment.Id, commentBody = comment.Body });
            }
            else
            {
                throw new NotFoundException($"There isn't a comment with id {updateBodyDTO.CommentId}");
            }

            return _mapper.Map<CommentDTO>(comment);
        }

        public async Task DeleteComment(int commentId, int userId)
        {
            var comment = _context.Comments.Where(comment => comment.Id == commentId)
                .FirstOrDefault();

            if (comment != null)
            {
                if (comment.AuthorId != userId)
                    throw new AccessViolationException(
                        $"User with Id = {userId} attempted to delete other user's comment");

                _context.Comments.Remove(comment);
                await _context.SaveChangesAsync();
                await _postHubContext.Clients.All.SendAsync("CommentDeleted", commentId);
            }
            else
            {
                throw new NotFoundException($"There isn't a comment with id {commentId}");
            }
        }
    }
}
