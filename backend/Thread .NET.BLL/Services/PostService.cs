﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.Common.DTO.User;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;
using Thread_.NET.ReusableUI.Areas.Emails.SharedViewModels;
using Thread_.NET.ReusableUI.Services;

namespace Thread_.NET.BLL.Services
{
    public sealed class PostService : BaseService
    {
        private readonly IHubContext<PostHub> _postHubContext;
        private readonly MailService _mailService;
        private readonly IRazorViewToStringRenderer _razorViewToStringRenderer;

        public PostService(
            ThreadContext context, 
            IMapper mapper, 
            IHubContext<PostHub> postHubContext,
            IRazorViewToStringRenderer razorViewToStringRenderer,
            MailService mailService) : base(context, mapper)
        {
            _postHubContext = postHubContext;
            _mailService = mailService;
            _razorViewToStringRenderer = razorViewToStringRenderer;
        }

        public async Task<ICollection<PostDTO>> GetAllPosts()
        {
            var posts = await _context.Posts
                .Include(p => p.Author)
                    .ThenInclude(a => a.Avatar)
                .Include(p => p.Preview)
                .Include(p => p.Reactions)
                    .ThenInclude(r => r.User)
                .Include(p => p.Comments)
                    .ThenInclude(c => c.Reactions)
                .Include(p => p.Comments)
                    .ThenInclude(c => c.Author)
                .OrderByDescending(p => p.CreatedAt)
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<ICollection<PostDTO>> GetAllPosts(int userId)
        {
            var posts = await _context.Posts
                .Include(p => p.Author)
                    .ThenInclude(a => a.Avatar)
                .Include(p => p.Preview)
                .Include(p => p.Comments)
                    .ThenInclude(c => c.Author)
                .Where(p => p.AuthorId == userId) // Filter here
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task DeletePost(int postId, int userId)
        {
            var post = _context.Posts.Where(p => p.Id == postId)
                .FirstOrDefault();

            if (post != null)
            {
                if (post.AuthorId != userId)
                    throw new AccessViolationException($"User with Id = {userId} attempted to delete other user's post");

                _context.Posts.Remove(post);
                await _context.SaveChangesAsync();
                await _postHubContext.Clients.All.SendAsync("PostDeleted", postId);
            }
            else
            {
                throw new NotFoundException($"There isn't a post with id {postId}");
            }    
        }

        public async Task SendPostByEmail(int currentUserId, SharePostDTO sharePostDTO)
        {
            var currentUser = await _context.Users
                .Include(u => u.Avatar)
                .FirstOrDefaultAsync(u => u.Id == currentUserId);

            var post = await _context.Posts
                .Include(p => p.Author)
                    .ThenInclude(a => a.Avatar)
                .Include(p => p.Preview)
                .FirstOrDefaultAsync(p => p.Id == sharePostDTO.PostId);

            if (post == null || currentUser == null)
            {
                throw new NotFoundException("Post or sender not found");
            }

            var userName = currentUser.UserName;
            var userAvatarUrl = currentUser.Avatar?.URL;

            var mailSubject = $"{userName.Substring(0, 1).ToUpper() + userName.Substring(1)} share a post from Thread .NET with you";
            var sharePostVM = new PostEmailViewModel(_mapper.Map<UserDTO>(currentUser), _mapper.Map<PostDTO>(post), userAvatarUrl);

            var body = await _razorViewToStringRenderer.RenderViewToStringAsync("/Views/Emails/SharePost/SharePostEmail.cshtml", sharePostVM);

            _mailService.SendEmail(sharePostDTO.ReceiverEmail, mailSubject, body, true);
        }

        public async Task<PostDTO> CreatePost(PostCreateDTO postDto)
        {
            var postEntity = _mapper.Map<Post>(postDto);

            _context.Posts.Add(postEntity);
            await _context.SaveChangesAsync();

            var createdPost = await _context.Posts
                .Include(p => p.Author)
					.ThenInclude(a => a.Avatar)
                .FirstAsync(p => p.Id == postEntity.Id);

            var createdPostDTO = _mapper.Map<PostDTO>(createdPost);
            await _postHubContext.Clients.All.SendAsync("NewPost", createdPostDTO);

            return createdPostDTO;
        }

        public async Task<PostDTO> UpdatePostBody(PostUpdateBodyDTO updateBodyDTO, int currentUserId)
        {
            var post = await _context.Posts
                .Include(p => p.Author)
                    .ThenInclude(a => a.Avatar)
                .Include(p => p.Preview)
                .Include(p => p.Comments)
                    .ThenInclude(c => c.Author)
                .FirstOrDefaultAsync(p => p.Id == updateBodyDTO.PostId);

            if (post != null)
            {
                if (currentUserId != post.AuthorId)
                    throw new AccessViolationException($"User with Id = {currentUserId} attempted to update other user's post");

                post.Body = updateBodyDTO.Body;
                await _context.SaveChangesAsync();
                var modifiedPostDTO = _mapper.Map<PostDTO>(post);
                await _postHubContext.Clients.All.SendAsync("PostBodyModified", 
                    new { postId = post.Id, postBody = post.Body });
            }
            else
            {
                throw new NotFoundException($"There isn't a post with id {updateBodyDTO.PostId}");
            }

            return _mapper.Map<PostDTO>(post);
        }
    }
}
