﻿using System;
using Thread_.NET.DAL.Entities.Abstract;

namespace Thread_.NET.DAL.Entities
{
    public sealed class RefreshToken : BaseEntity
    {
        private const int DAYS_TO_EXPIRE = 5;

        public string Token { get; set; }
        public DateTime Expires { get; private set; } = DateTime.UtcNow.AddDays(DAYS_TO_EXPIRE);

        public int UserId { get; set; }
        public User User { get; set; }

        public bool IsActive => DateTime.UtcNow <= Expires;
    }
}
