﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class ReactionsAndCommentsCascadeDeleting : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CommentReactions_Comments_CommentId",
                table: "CommentReactions");

            migrationBuilder.DropForeignKey(
                name: "FK_CommentReactions_Users_UserId",
                table: "CommentReactions");

            migrationBuilder.DropForeignKey(
                name: "FK_Comments_Users_AuthorId",
                table: "Comments");

            migrationBuilder.DropForeignKey(
                name: "FK_Comments_Posts_PostId",
                table: "Comments");

            migrationBuilder.DropForeignKey(
                name: "FK_PostReactions_Posts_PostId",
                table: "PostReactions");

            migrationBuilder.DropForeignKey(
                name: "FK_PostReactions_Users_UserId",
                table: "PostReactions");

            migrationBuilder.DropForeignKey(
                name: "FK_Posts_Users_AuthorId",
                table: "Posts");

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, 6, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5516), true, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5521), 2 },
                    { 2, 8, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5004), false, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5021), 14 },
                    { 3, 11, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5057), true, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5062), 16 },
                    { 4, 10, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5090), true, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5095), 10 },
                    { 5, 20, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5119), false, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5124), 4 },
                    { 6, 8, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5146), true, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5151), 12 },
                    { 7, 19, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5172), false, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5177), 16 },
                    { 8, 11, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5199), false, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5204), 13 },
                    { 9, 12, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5227), false, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5231), 9 },
                    { 10, 4, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5253), true, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5258), 14 },
                    { 1, 14, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(3905), true, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(4575), 3 },
                    { 12, 6, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5306), true, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5311), 13 },
                    { 13, 8, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5333), true, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5337), 15 },
                    { 14, 17, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5360), false, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5364), 18 },
                    { 15, 6, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5386), false, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5391), 19 },
                    { 16, 4, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5412), true, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5417), 15 },
                    { 17, 16, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5438), false, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5443), 10 },
                    { 18, 12, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5464), true, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5469), 13 },
                    { 19, 11, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5491), true, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5495), 7 },
                    { 11, 13, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5281), true, new DateTime(2020, 6, 11, 12, 50, 40, 330, DateTimeKind.Local).AddTicks(5285), 2 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Non excepturi reiciendis libero dolores nisi quo architecto praesentium.", new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(6097), 6, new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(6773) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Eius est nisi quidem.", new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(7308), 7, new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(7324) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Tempore velit nesciunt recusandae.", new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(7391), 19, new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(7397) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Consectetur sed temporibus dolor non.", new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(7517), 15, new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(7524) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Adipisci qui quidem non.", new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(7572), 16, new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(7578) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Veritatis doloremque nostrum enim eum libero.", new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(7633), 19, new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(7638) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Optio non quos molestias animi.", new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(7688), 12, new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(7694) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Id est voluptatem corporis.", new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(7744), 6, new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(7749) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 3, "Similique sint alias velit velit repellat.", new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(7804), new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(7809) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Ut eos consequuntur rerum et.", new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(7858), 14, new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(7863) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Est dolores deleniti.", new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(8007), 5, new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(8014) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Laborum quam error et.", new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(8068), 2, new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(8073) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Aut omnis placeat voluptas nihil accusamus nam omnis.", new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(8138), 6, new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(8144) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Qui qui et laudantium est itaque nihil.", new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(8202), 2, new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(8208) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Eligendi temporibus et at.", new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(8253), 6, new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(8259) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Asperiores quidem similique molestiae esse nihil.", new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(8309), 8, new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(8314) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Et quibusdam cum doloremque aliquam rerum.", new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(8365), 18, new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(8371) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Et voluptatibus voluptatem est.", new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(8469), 15, new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(8475) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Voluptatibus molestias voluptate.", new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(8516), 20, new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(8521) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Ut culpa ad assumenda dicta qui expedita eaque iusto beatae.", new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(8594), 6, new DateTime(2020, 6, 11, 12, 50, 40, 317, DateTimeKind.Local).AddTicks(8599) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 129, DateTimeKind.Local).AddTicks(6489), "https://s3.amazonaws.com/uifaces/faces/twitter/malykhinv/128.jpg", new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(688) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1225), "https://s3.amazonaws.com/uifaces/faces/twitter/jagan123/128.jpg", new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1247) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1277), "https://s3.amazonaws.com/uifaces/faces/twitter/danillos/128.jpg", new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1281) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1301), "https://s3.amazonaws.com/uifaces/faces/twitter/dixchen/128.jpg", new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1305) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1325), "https://s3.amazonaws.com/uifaces/faces/twitter/sindresorhus/128.jpg", new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1329) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1346), "https://s3.amazonaws.com/uifaces/faces/twitter/s4f1/128.jpg", new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1351) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1369), "https://s3.amazonaws.com/uifaces/faces/twitter/axel/128.jpg", new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1374) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1393), "https://s3.amazonaws.com/uifaces/faces/twitter/mr_subtle/128.jpg", new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1397) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1415), "https://s3.amazonaws.com/uifaces/faces/twitter/yalozhkin/128.jpg", new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1420) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1439), "https://s3.amazonaws.com/uifaces/faces/twitter/shanehudson/128.jpg", new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1444) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1524), "https://s3.amazonaws.com/uifaces/faces/twitter/xripunov/128.jpg", new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1528) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1548), "https://s3.amazonaws.com/uifaces/faces/twitter/marakasina/128.jpg", new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1552) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1571), "https://s3.amazonaws.com/uifaces/faces/twitter/Talbi_ConSept/128.jpg", new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1576) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1595), "https://s3.amazonaws.com/uifaces/faces/twitter/samscouto/128.jpg", new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1600) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1618), "https://s3.amazonaws.com/uifaces/faces/twitter/nathalie_fs/128.jpg", new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1623) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1643), "https://s3.amazonaws.com/uifaces/faces/twitter/tereshenkov/128.jpg", new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1648) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1666), "https://s3.amazonaws.com/uifaces/faces/twitter/marciotoledo/128.jpg", new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1671) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1688), "https://s3.amazonaws.com/uifaces/faces/twitter/dpmachado/128.jpg", new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1693) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1711), "https://s3.amazonaws.com/uifaces/faces/twitter/aluisio_azevedo/128.jpg", new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1716) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1733), "https://s3.amazonaws.com/uifaces/faces/twitter/aroon_sharma/128.jpg", new DateTime(2020, 6, 11, 12, 50, 40, 130, DateTimeKind.Local).AddTicks(1737) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 134, DateTimeKind.Local).AddTicks(9301), "https://picsum.photos/640/480/?image=895", new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(2) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(238), "https://picsum.photos/640/480/?image=148", new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(264) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(289), "https://picsum.photos/640/480/?image=1047", new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(294) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(452), "https://picsum.photos/640/480/?image=785", new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(459) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(481), "https://picsum.photos/640/480/?image=800", new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(485) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(503), "https://picsum.photos/640/480/?image=995", new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(508) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(525), "https://picsum.photos/640/480/?image=892", new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(530) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(547), "https://picsum.photos/640/480/?image=396", new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(551) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(568), "https://picsum.photos/640/480/?image=377", new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(573) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(589), "https://picsum.photos/640/480/?image=576", new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(594) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(612), "https://picsum.photos/640/480/?image=61", new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(617) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(635), "https://picsum.photos/640/480/?image=16", new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(639) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(657), "https://picsum.photos/640/480/?image=721", new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(661) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(678), "https://picsum.photos/640/480/?image=497", new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(683) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(700), "https://picsum.photos/640/480/?image=325", new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(705) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(722), "https://picsum.photos/640/480/?image=52", new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(727) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(786), "https://picsum.photos/640/480/?image=75", new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(791) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(810), "https://picsum.photos/640/480/?image=799", new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(814) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(831), "https://picsum.photos/640/480/?image=1013", new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(836) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(853), "https://picsum.photos/640/480/?image=205", new DateTime(2020, 6, 11, 12, 50, 40, 135, DateTimeKind.Local).AddTicks(857) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 10, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(7948), true, 3, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(7953), 6 },
                    { 11, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(7992), false, 1, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(7997), 20 },
                    { 12, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(8019), true, 17, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(8023), 11 },
                    { 13, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(8045), true, 13, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(8049), 20 },
                    { 15, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(8098), true, 6, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(8103), 2 },
                    { 18, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(8177), true, 15, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(8182), 7 },
                    { 16, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(8124), true, 2, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(8129), 12 },
                    { 17, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(8152), false, 13, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(8156), 16 },
                    { 9, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(7921), false, 16, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(7925), 11 },
                    { 20, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(8230), true, 8, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(8234), 7 },
                    { 19, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(8204), false, 10, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(8208), 7 },
                    { 14, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(8071), false, 1, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(8076), 9 },
                    { 8, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(7894), false, 2, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(7898), 10 },
                    { 3, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(7753), false, 15, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(7758), 19 },
                    { 6, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(7839), false, 4, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(7844), 10 },
                    { 5, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(7811), true, 17, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(7816), 1 },
                    { 1, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(6456), true, 11, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(7140), 20 },
                    { 4, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(7782), true, 5, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(7787), 7 },
                    { 2, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(7701), true, 17, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(7718), 19 },
                    { 7, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(7867), false, 9, new DateTime(2020, 6, 11, 12, 50, 40, 324, DateTimeKind.Local).AddTicks(7871), 8 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "dicta", new DateTime(2020, 6, 11, 12, 50, 40, 310, DateTimeKind.Local).AddTicks(3238), 30, new DateTime(2020, 6, 11, 12, 50, 40, 310, DateTimeKind.Local).AddTicks(3925) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "in", new DateTime(2020, 6, 11, 12, 50, 40, 310, DateTimeKind.Local).AddTicks(4605), 36, new DateTime(2020, 6, 11, 12, 50, 40, 310, DateTimeKind.Local).AddTicks(4619) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "est", new DateTime(2020, 6, 11, 12, 50, 40, 310, DateTimeKind.Local).AddTicks(4671), 40, new DateTime(2020, 6, 11, 12, 50, 40, 310, DateTimeKind.Local).AddTicks(4676) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "Atque modi autem maxime inventore ut voluptatem tempora.", new DateTime(2020, 6, 11, 12, 50, 40, 310, DateTimeKind.Local).AddTicks(9239), 26, new DateTime(2020, 6, 11, 12, 50, 40, 310, DateTimeKind.Local).AddTicks(9264) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Dolorem enim sit. Autem amet perspiciatis voluptatibus sint. Sed dolor possimus in ex accusamus saepe atque optio. Repellat laboriosam dolores autem assumenda aut dolores. Nulla quibusdam dolor culpa. Ipsam sapiente et aut.", new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(2992), 30, new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(3014) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "Delectus quam officia similique corporis fugiat nam ex. Et consequuntur suscipit vitae corrupti eos. Quas illum illo iure dolores porro. Fuga aperiam odio odio et eos qui quis. Consequatur ipsam quis quo sed sed tempore ullam.", new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(3382), 30, new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(3391) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Blanditiis consequatur incidunt voluptatum exercitationem.", new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(3455), 33, new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(3461) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "Suscipit unde magnam autem delectus provident eos qui quod dolores.", new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(3531), 40, new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(3537) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, "Assumenda fugit nihil amet ullam ratione facere.", new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(3648), 38, new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(3655) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "Dolorem modi quia sapiente et.", new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(3709), 25, new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(3714) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "Voluptas quia et ut tempore consequatur. Placeat asperiores ea eos quasi veniam laudantium ea officiis iusto. Incidunt voluptatem iusto et recusandae ut et nemo. Et fugit quaerat mollitia id omnis inventore consequatur. Et hic et suscipit pariatur voluptatem et repudiandae reprehenderit aspernatur. Maxime id possimus aut impedit soluta soluta aperiam eos consectetur.", new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(4030), 28, new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(4037) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "Totam harum id consequatur.", new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(4090), 26, new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(4096) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "blanditiis", new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(4172), 28, new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(4177) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "Saepe corporis quis.", new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(4257), 26, new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(4263) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 20, "nihil", new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(4298), new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(4303) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Necessitatibus et accusamus voluptatem ratione et. Excepturi voluptatem dolor. Sint sint aut dolores culpa quo. Ut iure ipsam rerum temporibus.", new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(4433), 28, new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(4439) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, @"Minus asperiores magnam rerum.
Minima et suscipit est sed.", new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(5274), 27, new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(5290) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Et quo et assumenda suscipit sint perferendis consequuntur et et.", new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(5376), 32, new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(5382) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "dolore", new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(5414), 23, new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(5420) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Quidem rerum dolor ut molestiae quod labore.", new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(5481), 33, new DateTime(2020, 6, 11, 12, 50, 40, 311, DateTimeKind.Local).AddTicks(5486) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 6, 11, 12, 50, 40, 165, DateTimeKind.Local).AddTicks(1886), "Helena6@hotmail.com", "LVIdAepdBZ6zW9nCBoW/S2ubIUT1xJxcjPFt4DIaCpQ=", "mJydyGTN1OQjn01QpWFjLdaU4PveGsJNvtrmJP5LtMg=", new DateTime(2020, 6, 11, 12, 50, 40, 165, DateTimeKind.Local).AddTicks(2724), "Gabriel_Moen" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2020, 6, 11, 12, 50, 40, 172, DateTimeKind.Local).AddTicks(2535), "Emil.Rutherford@hotmail.com", "ExZ12cEEv0U5DdZ+2xBhzQ1HBK2/RHvh22CMIgOTsh0=", "choKALmjCtkm4uCVz+AO7erxEzI6jWYQ6amMnseohHI=", new DateTime(2020, 6, 11, 12, 50, 40, 172, DateTimeKind.Local).AddTicks(2565), "Terence.Erdman" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2020, 6, 11, 12, 50, 40, 179, DateTimeKind.Local).AddTicks(1036), "Cristal85@hotmail.com", "QaYdzapDiD47Gc1BNJkMij8U2QjaU8mQ1jau1Amuz2I=", "MtxEQT5h6NeUwdT1cd3SjjZ3DOks7l5RFSi5JaCF+Yg=", new DateTime(2020, 6, 11, 12, 50, 40, 179, DateTimeKind.Local).AddTicks(1054), "Consuelo_Herzog92" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2020, 6, 11, 12, 50, 40, 185, DateTimeKind.Local).AddTicks(9546), "Breanna89@hotmail.com", "4nSc28pgBjRIADJOw7auCMUz1VROvQNnDdG8yyCIEt4=", "QG7O1y6YpkTAeNI1Lys0fKFkCQ3WsTfQQdFDEY3dng0=", new DateTime(2020, 6, 11, 12, 50, 40, 185, DateTimeKind.Local).AddTicks(9559), "Billie_Greenholt99" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2020, 6, 11, 12, 50, 40, 192, DateTimeKind.Local).AddTicks(7835), "Jeanne56@hotmail.com", "Wg8DvzyLBYbVZRYtXCDGw3cL7Al6fXNN4yKoQ/bqqI8=", "+efYlwANLALkN89zQIsGmTPAukK6FbWUupEnRBJoXss=", new DateTime(2020, 6, 11, 12, 50, 40, 192, DateTimeKind.Local).AddTicks(7847), "Deangelo66" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2020, 6, 11, 12, 50, 40, 199, DateTimeKind.Local).AddTicks(6043), "Luz.Towne22@gmail.com", "bLZ2EeOIzinNpdJt6aPGWfqy9RK5JsnG7pX2hZ5R3jA=", "L110wNsv2a15Uf1ZXHL1o2NzrNcDverLB6dq9f4D4EM=", new DateTime(2020, 6, 11, 12, 50, 40, 199, DateTimeKind.Local).AddTicks(6056), "Angelina.Schumm26" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2020, 6, 11, 12, 50, 40, 206, DateTimeKind.Local).AddTicks(4702), "Lorena71@yahoo.com", "hacnkDtZmBph7Zok7lS3l0aNlHlTqZTX4l89fxg08vM=", "hW1PPwNnjSGuU/F9BjK1WF/3TX5NVo6IGSisgXTFPwE=", new DateTime(2020, 6, 11, 12, 50, 40, 206, DateTimeKind.Local).AddTicks(4730), "Robin_Hoppe" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2020, 6, 11, 12, 50, 40, 213, DateTimeKind.Local).AddTicks(3472), "Eriberto_Botsford@yahoo.com", "Ik85p/CYuxiV2Bja/9EW+AkYZES/yqHovf+pCUf+pCs=", "fZYlNqr8D6vAfvADrEK8r9PThffVhpcKZZr8+q3Wmd4=", new DateTime(2020, 6, 11, 12, 50, 40, 213, DateTimeKind.Local).AddTicks(3489), "Jean_Becker" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2020, 6, 11, 12, 50, 40, 220, DateTimeKind.Local).AddTicks(1877), "Maryjane81@yahoo.com", "v2c8hs6RZlKelAgTD5OU68SbpBj0lF8+Mm6AAQPeyVI=", "aSO7a11eSuUX/MzjdFoaCEkg4gOJjMdn0KcR+EcYAvA=", new DateTime(2020, 6, 11, 12, 50, 40, 220, DateTimeKind.Local).AddTicks(1889), "Rashad72" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2020, 6, 11, 12, 50, 40, 227, DateTimeKind.Local).AddTicks(255), "Noe36@yahoo.com", "rzM0jNQ6vEvBXLVa2Yt9ywpLiYOIwdy3e4nY8LPNiJg=", "n8+58MdIHK43BuYwrjYgqathmRGk2pg1SkgM+G+VInk=", new DateTime(2020, 6, 11, 12, 50, 40, 227, DateTimeKind.Local).AddTicks(269), "Savion.Hansen" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2020, 6, 11, 12, 50, 40, 233, DateTimeKind.Local).AddTicks(8824), "Bryce.Herzog94@hotmail.com", "U8bdttlJqIqeT3gPJcJxsAlBmtxjOZJsCHUBckeXkVc=", "/8OA46pJ4ZFwTUL+x/KGJtT4GAnqgJ808mErTcNdMSA=", new DateTime(2020, 6, 11, 12, 50, 40, 233, DateTimeKind.Local).AddTicks(8845), "Nikolas_Kris46" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2020, 6, 11, 12, 50, 40, 240, DateTimeKind.Local).AddTicks(7316), "Sheila_Kreiger@hotmail.com", "TZwvs0FCHv/vAsD+mE3BKCi0DemZqGRRxOyXTv6r86g=", "0ObgSpmrhKUjouUFcozQL/xElheDMYGHX0AeXNsKLS0=", new DateTime(2020, 6, 11, 12, 50, 40, 240, DateTimeKind.Local).AddTicks(7342), "Eino_Brekke" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2020, 6, 11, 12, 50, 40, 248, DateTimeKind.Local).AddTicks(4158), "Hayley.Fadel77@yahoo.com", "DPswJZrBPF4otqxE21JUPDnUpiWZSFMXrnmLR6KHDTk=", "KtbOywatjl2SWA9nKnwKhBPVTSjY6ahaUdV5fhiB+7k=", new DateTime(2020, 6, 11, 12, 50, 40, 248, DateTimeKind.Local).AddTicks(4207), "Maci.Kemmer17" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2020, 6, 11, 12, 50, 40, 255, DateTimeKind.Local).AddTicks(2805), "Lora_Hyatt@yahoo.com", "S4/iQubSbZ0j7TbcpU6jMD6gWWjf312H7SPFo1y2BiA=", "AqPBI9ZJLDekJTz5bvjARjY1z+kz9Jfn8Jvd97i+qHk=", new DateTime(2020, 6, 11, 12, 50, 40, 255, DateTimeKind.Local).AddTicks(2817), "Seth.Hammes" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2020, 6, 11, 12, 50, 40, 262, DateTimeKind.Local).AddTicks(3065), "Brooklyn.Steuber@gmail.com", "USdH7Sefes0IdHb0Fv68ETnfCRpKCmuouhtCBrSj3fk=", "G8zQosaS28DWB7jM+/PV/aHHu2mecoEmmdqMzAVKpZ0=", new DateTime(2020, 6, 11, 12, 50, 40, 262, DateTimeKind.Local).AddTicks(3096), "Rhoda.Jakubowski54" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2020, 6, 11, 12, 50, 40, 269, DateTimeKind.Local).AddTicks(1442), "Amina78@yahoo.com", "78XdanXnaV8XSrYNI41n3Q34H8hkCHdMN4LNLehegVw=", "DvyNoLn/HUgYt2nL1DgxmApGBiiszOsK353PDZBqacQ=", new DateTime(2020, 6, 11, 12, 50, 40, 269, DateTimeKind.Local).AddTicks(1455), "Abbey67" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2020, 6, 11, 12, 50, 40, 275, DateTimeKind.Local).AddTicks(9881), "Amina_Goyette70@hotmail.com", "MTjMZI1E4lyyvRVHWluxACk1JLqAr74b7/eYeJ/Ievc=", "k5X4DSxNgVscQQTjLtBsw0ES0jyakrolqvMOPQed/24=", new DateTime(2020, 6, 11, 12, 50, 40, 275, DateTimeKind.Local).AddTicks(9898), "Cara_McGlynn64" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2020, 6, 11, 12, 50, 40, 282, DateTimeKind.Local).AddTicks(8302), "Keanu.Koss6@gmail.com", "4wPhalPXDytr7CwFvrsQhl4l+K0RyMc89C71RI5dvpQ=", "9Czi2mPiP/2+RPHPaNs1InD4AUR6EqCrGBK/ELi473o=", new DateTime(2020, 6, 11, 12, 50, 40, 282, DateTimeKind.Local).AddTicks(8324), "Clay46" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 289, DateTimeKind.Local).AddTicks(6535), "Dallin.Bergnaum43@gmail.com", "YOrkRVxa5OO6oagZS2PU7bm6j1m/1fmCTdnj1N/p8jM=", "bkN3NYEu++A6ch38R3Q+DMH0lJzbtW/Vdf9HVLO02Ms=", new DateTime(2020, 6, 11, 12, 50, 40, 289, DateTimeKind.Local).AddTicks(6546), "Jada.Flatley36" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2020, 6, 11, 12, 50, 40, 296, DateTimeKind.Local).AddTicks(4855), "Amiya60@hotmail.com", "S04PjzG7rxPj6bMbXjXadmK2N27CvOyWaIWTKv+dVxk=", "2MiLkq4TLHrVbFFRUg4NgGhyAAP7LHZdGdd886a/riI=", new DateTime(2020, 6, 11, 12, 50, 40, 296, DateTimeKind.Local).AddTicks(4887), "Lindsay_Russel55" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 11, 12, 50, 40, 303, DateTimeKind.Local).AddTicks(3765), "1/4kfHVgsnfuUWdwJe8k2n2wBb6a2+TNwxEmaS5BxVU=", "ysFA56UmPDQXI5Ikm79xn53BAIQ/ACIWdQDnpkNd6l0=", new DateTime(2020, 6, 11, 12, 50, 40, 303, DateTimeKind.Local).AddTicks(3765) });

            migrationBuilder.AddForeignKey(
                name: "FK_CommentReactions_Comments_CommentId",
                table: "CommentReactions",
                column: "CommentId",
                principalTable: "Comments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CommentReactions_Users_UserId",
                table: "CommentReactions",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_Users_AuthorId",
                table: "Comments",
                column: "AuthorId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_Posts_PostId",
                table: "Comments",
                column: "PostId",
                principalTable: "Posts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PostReactions_Posts_PostId",
                table: "PostReactions",
                column: "PostId",
                principalTable: "Posts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PostReactions_Users_UserId",
                table: "PostReactions",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Posts_Users_AuthorId",
                table: "Posts",
                column: "AuthorId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CommentReactions_Comments_CommentId",
                table: "CommentReactions");

            migrationBuilder.DropForeignKey(
                name: "FK_CommentReactions_Users_UserId",
                table: "CommentReactions");

            migrationBuilder.DropForeignKey(
                name: "FK_Comments_Users_AuthorId",
                table: "Comments");

            migrationBuilder.DropForeignKey(
                name: "FK_Comments_Posts_PostId",
                table: "Comments");

            migrationBuilder.DropForeignKey(
                name: "FK_PostReactions_Posts_PostId",
                table: "PostReactions");

            migrationBuilder.DropForeignKey(
                name: "FK_PostReactions_Users_UserId",
                table: "PostReactions");

            migrationBuilder.DropForeignKey(
                name: "FK_Posts_Users_AuthorId",
                table: "Posts");

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, 11, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(2190), true, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(2202), 2 },
                    { 2, 18, new DateTime(2019, 6, 8, 13, 30, 1, 865, DateTimeKind.Local).AddTicks(9981), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(9), 6 },
                    { 3, 14, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(115), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(127), 18 },
                    { 4, 18, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(208), true, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(224), 15 },
                    { 5, 18, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(305), true, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(321), 2 },
                    { 6, 8, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(402), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(419), 14 },
                    { 7, 10, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(504), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(520), 12 },
                    { 8, 1, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(666), true, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(682), 19 },
                    { 9, 3, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(779), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(796), 16 },
                    { 10, 13, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(881), true, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(897), 15 },
                    { 1, 11, new DateTime(2019, 6, 8, 13, 30, 1, 865, DateTimeKind.Local).AddTicks(6993), false, new DateTime(2019, 6, 8, 13, 30, 1, 865, DateTimeKind.Local).AddTicks(8315), 21 },
                    { 12, 20, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1079), true, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1096), 9 },
                    { 13, 6, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1185), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1197), 1 },
                    { 14, 18, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1282), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1298), 7 },
                    { 15, 20, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1379), true, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1396), 13 },
                    { 16, 4, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1797), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1821), 12 },
                    { 17, 17, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1915), true, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1931), 1 },
                    { 18, 7, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(2008), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(2024), 18 },
                    { 19, 2, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(2101), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(2113), 10 },
                    { 11, 18, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(978), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(994), 5 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Et minus amet.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(961), 3, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(2177) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Sit sint architecto.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(4079), 13, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(4119) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Ea ad deleniti ut quis officia voluptatibus occaecati.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(4379), 7, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(4399) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "In eius qui necessitatibus et sapiente quis iure.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(4626), 16, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(4642) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Dolor doloremque est rerum et quis.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(4837), 8, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(4853) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Tenetur occaecati omnis dolorem molestiae.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5039), 6, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5056) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Ut tempore ut id et odit temporibus.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5295), 7, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5315) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Qui iste temporibus dolores.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5494), 20, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5514) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 19, "Dolores ducimus magni qui nesciunt est quia aut a tempore.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5729), new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5749) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Delectus non nihil cumque sed dolores ut impedit.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5952), 19, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5972) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Consequuntur odit tempora omnis vel sunt illum nobis et quia.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(6195), 10, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(6211) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "At aut veritatis veritatis.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(6402), 20, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(6418) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Voluptates inventore libero sit illo pariatur.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(6657), 1, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(6677) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Quam magnam quidem cupiditate ratione id ab eum.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(6904), 4, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(6925) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "At adipisci expedita dignissimos provident architecto.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(7119), 17, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(7135) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Architecto deleniti earum.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(7285), 10, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(7302) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Quasi iusto ipsa.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(7460), 20, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(7480) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Aut maiores eum a quibusdam non.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(7837), 6, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(7861) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Soluta est animi.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(8072), 12, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(8092) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "In tempora et voluptatibus ut et consequatur fuga.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(8287), 7, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(8303) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 425, DateTimeKind.Local).AddTicks(742), "https://s3.amazonaws.com/uifaces/faces/twitter/exentrich/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(2661) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(5920), "https://s3.amazonaws.com/uifaces/faces/twitter/kolage/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(5952) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6046), "https://s3.amazonaws.com/uifaces/faces/twitter/joshaustin/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6058) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6127), "https://s3.amazonaws.com/uifaces/faces/twitter/yecidsm/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6143) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6212), "https://s3.amazonaws.com/uifaces/faces/twitter/aoimedia/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6224) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6293), "https://s3.amazonaws.com/uifaces/faces/twitter/jonkspr/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6305) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6374), "https://s3.amazonaws.com/uifaces/faces/twitter/mutlu82/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6386) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6459), "https://s3.amazonaws.com/uifaces/faces/twitter/mgonto/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6471) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6540), "https://s3.amazonaws.com/uifaces/faces/twitter/karalek/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6552) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6621), "https://s3.amazonaws.com/uifaces/faces/twitter/igorgarybaldi/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6633) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6698), "https://s3.amazonaws.com/uifaces/faces/twitter/alek_djuric/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6715) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6779), "https://s3.amazonaws.com/uifaces/faces/twitter/renbyrd/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6796) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6860), "https://s3.amazonaws.com/uifaces/faces/twitter/mahmoudmetwally/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6873) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6942), "https://s3.amazonaws.com/uifaces/faces/twitter/mattdetails/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6954) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7019), "https://s3.amazonaws.com/uifaces/faces/twitter/baumann_alex/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7035) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7100), "https://s3.amazonaws.com/uifaces/faces/twitter/kuldarkalvik/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7116) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7181), "https://s3.amazonaws.com/uifaces/faces/twitter/motionthinks/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7193) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7258), "https://s3.amazonaws.com/uifaces/faces/twitter/urrutimeoli/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7270) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7339), "https://s3.amazonaws.com/uifaces/faces/twitter/wesleytrankin/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7355) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7420), "https://s3.amazonaws.com/uifaces/faces/twitter/timmillwood/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7436) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(539), "https://picsum.photos/640/480/?image=393", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(1638) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2112), "https://picsum.photos/640/480/?image=1079", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2141) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2218), "https://picsum.photos/640/480/?image=801", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2234) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2299), "https://picsum.photos/640/480/?image=951", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2315) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2376), "https://picsum.photos/640/480/?image=436", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2392) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2457), "https://picsum.photos/640/480/?image=64", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2473) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2538), "https://picsum.photos/640/480/?image=436", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2550) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2639), "https://picsum.photos/640/480/?image=243", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2656) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2720), "https://picsum.photos/640/480/?image=515", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2733) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2797), "https://picsum.photos/640/480/?image=290", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2814) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2879), "https://picsum.photos/640/480/?image=400", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2891) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2956), "https://picsum.photos/640/480/?image=839", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2972) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3037), "https://picsum.photos/640/480/?image=926", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3053) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3118), "https://picsum.photos/640/480/?image=163", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3130) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3195), "https://picsum.photos/640/480/?image=455", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3207) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3272), "https://picsum.photos/640/480/?image=688", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3284) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3349), "https://picsum.photos/640/480/?image=759", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3361) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3430), "https://picsum.photos/640/480/?image=307", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3442) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3507), "https://picsum.photos/640/480/?image=488", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3523) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3584), "https://picsum.photos/640/480/?image=902", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3600) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 10, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4275), false, 5, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4287), 15 },
                    { 11, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4364), false, 9, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4381), 8 },
                    { 12, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4458), false, 18, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4470), 2 },
                    { 13, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4543), false, 10, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4559), 16 },
                    { 15, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4721), false, 11, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4733), 12 },
                    { 18, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(5001), true, 3, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(5017), 5 },
                    { 16, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4806), true, 19, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4822), 16 },
                    { 17, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4904), false, 14, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4916), 15 },
                    { 9, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4186), false, 8, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4198), 5 },
                    { 20, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(5199), false, 8, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(5216), 19 },
                    { 19, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(5098), false, 3, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(5114), 10 },
                    { 14, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4632), true, 9, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4644), 20 },
                    { 8, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4097), true, 8, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4113), 20 },
                    { 3, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3606), false, 18, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3623), 5 },
                    { 6, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3906), false, 19, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3918), 13 },
                    { 5, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3809), true, 3, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3821), 16 },
                    { 1, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(465), false, 2, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(1693), 1 },
                    { 4, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3716), true, 6, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3728), 5 },
                    { 2, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3452), false, 7, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3485), 13 },
                    { 7, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4004), true, 7, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4020), 18 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Enim ea sunt eos reprehenderit maxime.", new DateTime(2019, 6, 8, 13, 30, 1, 808, DateTimeKind.Local).AddTicks(9179), 22, new DateTime(2019, 6, 8, 13, 30, 1, 809, DateTimeKind.Local).AddTicks(346) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, @"Exercitationem assumenda expedita eos possimus dolorem aut repellat eos.
Error voluptate molestiae et laudantium.
Explicabo ea est totam soluta esse cumque voluptatibus qui voluptatem.
Ex corrupti praesentium omnis vitae occaecati.
Dicta doloribus qui vero sit incidunt id aspernatur ipsum.", new DateTime(2019, 6, 8, 13, 30, 1, 810, DateTimeKind.Local).AddTicks(4781), 23, new DateTime(2019, 6, 8, 13, 30, 1, 810, DateTimeKind.Local).AddTicks(4826) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "Non placeat asperiores qui. Quis qui aut. Tempore iste dolores.", new DateTime(2019, 6, 8, 13, 30, 1, 810, DateTimeKind.Local).AddTicks(9731), 34, new DateTime(2019, 6, 8, 13, 30, 1, 810, DateTimeKind.Local).AddTicks(9760) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, @"Atque rerum consequuntur illum rem quia labore.
Vel eos qui et sunt.
Officia aspernatur magnam molestiae.
Laudantium a voluptatibus ut expedita sed et ratione.
Cupiditate unde vitae officia quos.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(307), 31, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(323) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "aperiam", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(1361), 29, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(1389) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, @"Quisquam nihil natus quidem dolores iusto.
Qui et aspernatur est numquam harum et est totam et.
Accusantium itaque vel eum dolor asperiores placeat.
Aut voluptatum et.
Ut neque impedit voluptatum voluptatem vel aspernatur error nihil.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(1941), 35, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(1957) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "sed", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(2131), 40, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(2147) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "Voluptas praesentium similique et voluptates unde illum libero necessitatibus facilis.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(2387), 32, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(2403) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Quos voluptas mollitia magni est quas.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(2581), 24, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(2597) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Consequuntur ipsam est. Quo autem dolor rem quia quia perferendis et. Inventore laborum saepe. Quis dolorem provident cum a dolore voluptas et occaecati.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(3027), 38, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(3047) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Dolorum adipisci facere repellendus incidunt quia maiores. Pariatur iure esse nisi. Voluptatum eum error quasi repellat. Aliquid et tempore quo quis dolorem quasi. Aut consectetur itaque aut optio quis soluta qui corporis.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(3530), 26, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(3550) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Qui adipisci voluptatem ratione voluptatem laborum doloribus commodi.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(3741), 40, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(3757) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "In distinctio hic ex. Et neque dignissimos odit maiores repudiandae. Quisquam ut in vitae non minima.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(4065), 36, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(4081) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Aliquid eligendi aut omnis vitae in et ut. Voluptas provident aut sit. Hic ut et eius et quo. Voluptate minus ut facilis. Veniam dignissimos cum doloribus deserunt et.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(4523), 38, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(4539) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 14, @"Et vitae ipsa velit facere.
Est consectetur asperiores natus rerum culpa autem qui in.
Illum totam rerum a similique voluptates libero sed.
Iste neque ipsum quis odit doloremque.
Quis harum officia dolores sed sint blanditiis facilis dicta.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(5058), new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(5074) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Corporis nulla adipisci natus ab occaecati et nihil cumque. Velit et id adipisci. Deleniti ut necessitatibus provident autem nostrum maxime. Voluptatem deleniti corporis perferendis ullam officiis eius. Qui voluptatum mollitia aliquam voluptatem.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(5545), 40, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(5561) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "molestias", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(5674), 24, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(5690) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Minima omnis quaerat ab est ducimus omnis voluptatem. Sunt iste nostrum non vitae placeat molestias ex eum. Saepe deserunt dignissimos qui earum minima. Quia fugiat voluptatem id quo illum sed.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(6116), 31, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(6132) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, @"Ut assumenda et et.
Voluptatem optio doloremque ipsa rerum nemo repellat quo.
Dignissimos ut magni fugiat quidem voluptatem est aut enim.
Ullam et eos quod ipsum reprehenderit rerum tempora possimus aut.
Porro reprehenderit explicabo illum sed et dolore.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(6643), 30, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(6659) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, @"Sapiente quis culpa velit commodi rerum iusto voluptatum neque.
Sunt sed eum nesciunt a vero.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(6931), 34, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(6943) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2019, 6, 8, 13, 30, 1, 498, DateTimeKind.Local).AddTicks(4064), "Lewis66@yahoo.com", "ipp+S75e/5LiuvKBM0nZrEQsP+7SMRMzqcaRN08Npeo=", "euuwrkqYzcRNJGdeN7pEtbfzLCvqO+eavPQlg+vBgT4=", new DateTime(2019, 6, 8, 13, 30, 1, 498, DateTimeKind.Local).AddTicks(5085), "Berta.Mann13" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2019, 6, 8, 13, 30, 1, 510, DateTimeKind.Local).AddTicks(8861), "Zula.Schultz0@gmail.com", "P8ThNsvBpgNPvmH1YqSUD2/vtQRDH/Lul3MQa1elkRI=", "oI1mrCZQsTc837g5oIG3HMPEvL9DrfDpegteKB4mOas=", new DateTime(2019, 6, 8, 13, 30, 1, 510, DateTimeKind.Local).AddTicks(8946), "Emiliano_Rice44" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2019, 6, 8, 13, 30, 1, 523, DateTimeKind.Local).AddTicks(1692), "Reagan60@yahoo.com", "0seLIYhjNnliKhex2dDOLL/USB49B2vjmm35kPNClV0=", "2VgpQTnja97w2gvln6xsIeJnhGCvbckR1fahcmznjBw=", new DateTime(2019, 6, 8, 13, 30, 1, 523, DateTimeKind.Local).AddTicks(1773), "Keely_Johnston" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2019, 6, 8, 13, 30, 1, 535, DateTimeKind.Local).AddTicks(2230), "Keon.Lang62@yahoo.com", "6U7+IRUGC7b0o9gvk13Hmjq7fqhQrz5LUeBrI7N8KFc=", "yfVqUaYEqo8GpvMHeBTVWEvVt8tbqBlOg6RdI2UT4os=", new DateTime(2019, 6, 8, 13, 30, 1, 535, DateTimeKind.Local).AddTicks(2311), "Odell91" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2019, 6, 8, 13, 30, 1, 547, DateTimeKind.Local).AddTicks(4604), "Itzel.Hintz@yahoo.com", "NuSFkBsHxCG6xLWi69+t4D0QQGJ6BVVhb9xbXZLdvuw=", "RhFEqCkXU6TJ/PlnhiUc1oPhH+KaEWnNBGn83ls6cpI=", new DateTime(2019, 6, 8, 13, 30, 1, 547, DateTimeKind.Local).AddTicks(4685), "Elmore_Zieme78" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2019, 6, 8, 13, 30, 1, 563, DateTimeKind.Local).AddTicks(166), "Lonie68@hotmail.com", "9kzThCD61VguYbsRHNtsljedbZNR1l25iA5h7tnglD4=", "urBn05zOW2W/z0SRh+ROKzQA8zstGURtGXA8s6Vb2a4=", new DateTime(2019, 6, 8, 13, 30, 1, 563, DateTimeKind.Local).AddTicks(255), "Alanis58" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2019, 6, 8, 13, 30, 1, 578, DateTimeKind.Local).AddTicks(2826), "Kristin97@gmail.com", "0V7MfOMBFz35VAxpeLP/VykoHmFr7Y6TMb7KmCsI/aY=", "7sMIRbE3J8XKIl6X9jKvzdPbJKIBm2ObprWHVvqbjCg=", new DateTime(2019, 6, 8, 13, 30, 1, 578, DateTimeKind.Local).AddTicks(3337), "Mallory.Lowe" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2019, 6, 8, 13, 30, 1, 591, DateTimeKind.Local).AddTicks(1073), "Brock.Morissette45@yahoo.com", "W0C3ON2PXLVca3hpSsshp9A731PcJZ/uhs6iqIEPNO0=", "rfSm1BxD/Cz3A0fKq9SM3lViajI8Rf0JUblIsycGTHk=", new DateTime(2019, 6, 8, 13, 30, 1, 591, DateTimeKind.Local).AddTicks(1166), "Adrian47" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2019, 6, 8, 13, 30, 1, 603, DateTimeKind.Local).AddTicks(309), "Chester.Botsford18@hotmail.com", "TD3rl13TivQ0isdJJdv3otUiNVS1qu95yZQACMf2xlw=", "D+kG+ztr9Id+AD59gxJdJygPRlt/ioTnMqMBlmFDyjE=", new DateTime(2019, 6, 8, 13, 30, 1, 603, DateTimeKind.Local).AddTicks(394), "Alysha_Kovacek" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2019, 6, 8, 13, 30, 1, 617, DateTimeKind.Local).AddTicks(9451), "Marcelina62@yahoo.com", "aLjZF/bDcOOeW7qCP9dwuSPumeOm9QRufFkUKft46+4=", "bh5Ft5t/KbW9P4fmQXyGpy+XgKou4wo7+Rdd1ySJm8Y=", new DateTime(2019, 6, 8, 13, 30, 1, 617, DateTimeKind.Local).AddTicks(9552), "Rupert_Kassulke55" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2019, 6, 8, 13, 30, 1, 639, DateTimeKind.Local).AddTicks(2043), "Shaylee79@yahoo.com", "Rh0T1eDmyKZIDhDBfPV7+xzjf1K+T84PENeigQVqr+E=", "bYVeC1RbOLLbVVmDf1eTE1dAGCKYfgEgYbNWtgYNIyk=", new DateTime(2019, 6, 8, 13, 30, 1, 639, DateTimeKind.Local).AddTicks(2132), "Gwendolyn_Boehm" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2019, 6, 8, 13, 30, 1, 661, DateTimeKind.Local).AddTicks(877), "Mauricio.Von33@yahoo.com", "UTni2loimGZ5Kti93HaEZ+fciZCwfO/ZaPOc44RxhhM=", "cYSMaR0B14+YVoFvlt8tjpPoZY2z5K5jIFb2xsHfxZs=", new DateTime(2019, 6, 8, 13, 30, 1, 661, DateTimeKind.Local).AddTicks(966), "Coleman_Bosco95" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2019, 6, 8, 13, 30, 1, 673, DateTimeKind.Local).AddTicks(2489), "Elton.Farrell22@hotmail.com", "DSLPWWQIc4TRCSD6QevyvmOV2Q8UikaYj+CawmvEYhM=", "Nd3AGdFKVWNX6lV8EepGfNXRRTrIbZt+crUIxwN7K4A=", new DateTime(2019, 6, 8, 13, 30, 1, 673, DateTimeKind.Local).AddTicks(2582), "Delores_Hauck92" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2019, 6, 8, 13, 30, 1, 685, DateTimeKind.Local).AddTicks(5677), "Sabryna13@hotmail.com", "2I1TP1TVsZiysC0zeVLdGzmL6bsTG5Yro1ZMoZPOVZs=", "2ttqWS2Z+EZN+C1+MQpRFtwsg1kh/bqEJOhp5GaQdo0=", new DateTime(2019, 6, 8, 13, 30, 1, 685, DateTimeKind.Local).AddTicks(5778), "Lucie32" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2019, 6, 8, 13, 30, 1, 702, DateTimeKind.Local).AddTicks(7746), "Ardith.Cummings95@yahoo.com", "XKm4g0rS/kMzXDlq1A0xd0Mj1UTKz78QZqEIjGiVzaI=", "joVmnahae87VaRCkiCZANdQn93d2cMmII5sssUek7AQ=", new DateTime(2019, 6, 8, 13, 30, 1, 702, DateTimeKind.Local).AddTicks(7844), "Benton99" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2019, 6, 8, 13, 30, 1, 724, DateTimeKind.Local).AddTicks(7786), "Wilfrid54@yahoo.com", "dymXvZziyvE784wDCRRuv+Kazm4duf2yUAZQJG9MFzw=", "6iWfzQsuyR5S7/Xx02k+yJV5y6UWvnNU4rUmZYTba58=", new DateTime(2019, 6, 8, 13, 30, 1, 724, DateTimeKind.Local).AddTicks(7875), "Shaylee.King" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2019, 6, 8, 13, 30, 1, 741, DateTimeKind.Local).AddTicks(6309), "Harold.Rowe71@yahoo.com", "LoU3WAtUBJTUjad+UJqki5rZ6n/M9omgb/3+yVk7vMw=", "n/uUFtMkyUefMnQNz/j6Vaqzr/osn7pBREHzWM8lIU8=", new DateTime(2019, 6, 8, 13, 30, 1, 741, DateTimeKind.Local).AddTicks(6406), "Cali.Waters" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2019, 6, 8, 13, 30, 1, 753, DateTimeKind.Local).AddTicks(8333), "Samanta_OReilly95@yahoo.com", "+YA8dDK1ZoLFTvfpnVQGzmhKXHRaAgN/fQjmZMVMGcM=", "Bhn9S4MMVApZjnGJUamRVV4ikTzCgdc/KBbTMldMsjY=", new DateTime(2019, 6, 8, 13, 30, 1, 753, DateTimeKind.Local).AddTicks(8426), "Baylee_Sipes42" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 765, DateTimeKind.Local).AddTicks(8563), "Rex64@hotmail.com", "JOICOM6OozRPh4VLEbxS/iJMsI1HhHyOdDtuG4tau78=", "EdoXXUo+awEKlbwKaH/IFKIzz1Ybyl69YYeYJACQ/nM=", new DateTime(2019, 6, 8, 13, 30, 1, 765, DateTimeKind.Local).AddTicks(8648), "Sharon32" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2019, 6, 8, 13, 30, 1, 779, DateTimeKind.Local).AddTicks(1504), "Brice_Abshire72@gmail.com", "D+HiHDuS3LIsqdZUuSEQeZ0VSpvsWiTYTKiW1JwGMPU=", "unJ1mBlcdh5pmmYITwm9s49RsQknqWgoUYsqdrOvjbI=", new DateTime(2019, 6, 8, 13, 30, 1, 779, DateTimeKind.Local).AddTicks(1590), "Kayla46" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 791, DateTimeKind.Local).AddTicks(1782), "i2TJ0z00NYlgYE1RckEz65RuduHNOi6XLzkY/zjls3M=", "OfXWhBQuNh3ZiHzV/LYq5X7zvFdI56jXiAJ/cQcbvJM=", new DateTime(2019, 6, 8, 13, 30, 1, 791, DateTimeKind.Local).AddTicks(1782) });

            migrationBuilder.AddForeignKey(
                name: "FK_CommentReactions_Comments_CommentId",
                table: "CommentReactions",
                column: "CommentId",
                principalTable: "Comments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CommentReactions_Users_UserId",
                table: "CommentReactions",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_Users_AuthorId",
                table: "Comments",
                column: "AuthorId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_Posts_PostId",
                table: "Comments",
                column: "PostId",
                principalTable: "Posts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PostReactions_Posts_PostId",
                table: "PostReactions",
                column: "PostId",
                principalTable: "Posts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PostReactions_Users_UserId",
                table: "PostReactions",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Posts_Users_AuthorId",
                table: "Posts",
                column: "AuthorId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
