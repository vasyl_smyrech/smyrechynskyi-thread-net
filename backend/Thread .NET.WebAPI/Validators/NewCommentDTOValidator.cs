﻿using FluentValidation;
using Thread_.NET.Common.DTO.Comment;

namespace Thread_.NET.Validators
    {
        public sealed class NewCommentDTOValidator : AbstractValidator<NewCommentDTO>
        {
            public NewCommentDTOValidator()
            {
                RuleFor(c => c.AuthorId)
                    .NotNull();

                RuleFor(c => c.PostId)
                    .NotNull();

                RuleFor(c => c.Body)
                    .NotEmpty()
                    .MaximumLength(1000)
                    .WithName("Comment content");
        }
        }
    }

