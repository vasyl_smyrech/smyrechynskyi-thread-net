﻿using FluentValidation;
using Thread_.NET.Common.DTO.User;

namespace Thread_.NET.Validators
{
    public sealed class UserLoginDTOValidator : AbstractValidator<UserLoginDTO>
    {
        public UserLoginDTOValidator()
        {
            RuleFor(u => u.Email)
                .NotEmpty()
                .EmailAddress();

            RuleFor(u => u.Password)
                .NotEmpty()
                .Length(4, 16)
                    .WithMessage("Password must be from 4 to 16 characters")
                .Matches(@"^[a-zA-Z0-9/ !@#$%^&*()_[\]{},.<>+=-]+$")
                    .WithMessage("Password should contain only characters, digits and symbols(!@#$%^&*()_[]{},.<>+=-)");
        }
    }
}
