﻿using FluentValidation;
using Thread_.NET.Common.DTO.Comment;

namespace Thread_.NET.Validators
{
    public sealed class CommentUpdateBodyDTOValidator : AbstractValidator<CommentUpdateBodyDTO>
    {
        public CommentUpdateBodyDTOValidator()
        {
            RuleFor(c => c.CommentId)
                .NotNull();

            RuleFor(c => c.Body)
                .NotEmpty()
                .MaximumLength(1000)
                .WithName("Comment content");
        }
    }
}