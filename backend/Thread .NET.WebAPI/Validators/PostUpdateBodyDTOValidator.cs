﻿using FluentValidation;
using Thread_.NET.Common.DTO.Post;

namespace Thread_.NET.Validators
{
    public sealed class PostUpdateBodyDTOValidator : AbstractValidator<PostUpdateBodyDTO>
    {
        public PostUpdateBodyDTOValidator()
        {
            RuleFor(p => p.PostId)
                .NotNull();

            RuleFor(p => p.Body)
                .NotEmpty()
                .MaximumLength(500)
                .WithName("Post text content");
        }
    }
}

