﻿using FluentValidation;
using System.Text.RegularExpressions;
using Thread_.NET.Common.DTO.User;

namespace Thread_.NET.Validators
{
    public sealed class UserRegisterDTOValidator : AbstractValidator<UserRegisterDTO>
    {
        public UserRegisterDTOValidator()
        {

            RuleFor(u => u.Avatar)
                .Matches(new Regex(@"(?i)(http(s)?:\/\/)?(\w{2,25}\.)+\w{3}([a-z0-9\-?=$-_.+!*()]+)(?i)", RegexOptions.Singleline));

            RuleFor(u => u.Email)
                .NotEmpty()
                .EmailAddress();

            RuleFor(u => u.UserName)
                .NotEmpty()
                    .WithMessage("Username is mandatory")
                .Length(3, 100)
                    .WithMessage("Username must be between 3 and 100 characters long")
                .Matches(@"^[a-zA-Z0-9 ]+$")
                    .WithMessage("Username must contain only characters, digits, and white-spaces");

            RuleFor(u => u.Password)
                .Length(4, 16)
                    .WithMessage("Password must be from 4 to 16 characters")
                .Matches(@"^[a-zA-Z0-9 /!@#$%^&*()_[\]{},.<>+=-]+$")
                    .WithMessage("Password must contain only characters, digits and symbols(!@#$%^&*()_[]{},.<>+=-)");
        }
    }
}
