﻿using FluentValidation;
using System.Text.RegularExpressions;
using Thread_.NET.Common.DTO.Post;

namespace Thread_.NET.Validators
{
    public sealed class PostCreateDTOValidator : AbstractValidator<PostCreateDTO>
    {
        public PostCreateDTOValidator()
        {
            RuleFor(p => p.AuthorId)
                .NotNull();

            RuleFor(p => p.PreviewImage)
                .NotEmpty()
                .Matches(new Regex(@"(?i)(http(s)?:\/\/)?(\w{2,25}\.)+\w{3}([a-z0-9\-?=$-_.+!*()]+)(?i)", RegexOptions.Singleline))
                .WithName("Post image");

            RuleFor(p => p.Body)
                .NotEmpty()
                .MaximumLength(500)
                .WithName("Post text content");
        }
    }
}

