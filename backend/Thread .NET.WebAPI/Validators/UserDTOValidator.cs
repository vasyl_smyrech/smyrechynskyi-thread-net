﻿using FluentValidation;
using System.Text.RegularExpressions;
using Thread_.NET.Common.DTO.User;

namespace Thread_.NET.Validators
{
    public sealed class UserDTOValidator : AbstractValidator<UserDTO>
    {
        public UserDTOValidator()
        {

            RuleFor(u => u.Avatar)
                 .Matches(new Regex(@"(?i)(http(s)?:\/\/)?(\w{2,25}\.)+\w{3}([a-z0-9\-?=$-_.+!*()]+)(?i)", RegexOptions.Singleline));

            RuleFor(u => u.Email)
                .EmailAddress();

            RuleFor(u => u.UserName)
                .NotEmpty()
                    .WithMessage("Username is mandatory")
                .Length(3, 100)
                    .WithMessage("Username should be between 3 and 100 characters long")
                .Matches(@"^[a-zA-Z0-9/ ]+$")
                    .WithMessage("Username should contain only characters, digits, and whitespases");
        }
    }
}