﻿using FluentValidation;
using Thread_.NET.Common.DTO.Like;

namespace Thread_.NET.Validators
{
    public sealed class NewReactionDTOValidator : AbstractValidator<NewReactionDTO>
    {
        public NewReactionDTOValidator()
        {
            RuleFor(r => r.EntityId)
                .NotNull();

            RuleFor(r => r.UserId)
                .NotNull();
        }
    }
}