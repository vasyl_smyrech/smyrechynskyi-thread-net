﻿using Thread_.NET.Common.DTO.User;

namespace Thread_.NET.ReusableUI.Areas.Emails.ResetPassword
{
    public class ResetPasswordEmailViewModel
    {
        public ResetPasswordEmailViewModel(UserDTO user, string resetPasswordUrl) 
        {
            User = user;
            ResetPasswordUrl = resetPasswordUrl;
        }

        public UserDTO User { get; }
        public string ResetPasswordUrl { get; }
    }
}
