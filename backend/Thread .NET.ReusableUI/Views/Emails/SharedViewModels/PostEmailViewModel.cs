﻿using Thread_.NET.Common.DTO.Post;
using Thread_.NET.Common.DTO.User;

namespace Thread_.NET.ReusableUI.Areas.Emails.SharedViewModels
{
    public class PostEmailViewModel
    {
        public PostEmailViewModel(UserDTO user, PostDTO post, string likedUserAvatarUrl)
        {
            User = user;
            Post = post;
            LikedUserAvatarUrl = likedUserAvatarUrl;
        }

        public UserDTO User { get; }
        public PostDTO Post { get; }
        public string LikedUserAvatarUrl { get; }
    }
}
