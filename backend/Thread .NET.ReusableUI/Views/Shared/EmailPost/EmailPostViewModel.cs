﻿using Thread_.NET.Common.DTO.Post;

namespace Thread_.NET.ReusableUI.Areas.Shared.EmailPost
{
    public class EmailPostViewModel
    {
        public EmailPostViewModel(PostDTO post)
        {
            Post = post;
        }

        public PostDTO Post { get; }
    }
}
